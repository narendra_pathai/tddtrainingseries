 1) Static causes leak of system state and does not ensure test isolation
 - Example Singleton with counter which fails on second call. Have two test methods, first will pass
 and second will fail unexpectedly. So that will teach lesson that statics leak state.
 
 2) Direct external calls or system dependencies are bad
 - `DreadfulSingleton.doSomething()` will call `DreadfulSingleton.init()` internally which
 will throw `DreadfulSystemNotAvailableException` to prove that we are crossing system boundaries and dependent
 on database. HibernateSessionFactory will have no interface, so first we will have to introduce SEAM.
 - Then use that seam to inject dependency from constructor
 - Lesson 1 - Atleast singletons should have interfaces
 
 3) In singleton example thing of some analogy of Rocket and RocketLauncher or RocketLaunchController
 
 4) Direct static call to dependency
 - `SNMPAgent.generateAlert(message)` from a class
 
 5) System.currentTime() is also external dependency example
 
 6) Chain Handler example from AAA source code
 
 7) Two styles of testing: Testing with state verification and Testing with behavior verification. Warehouse and order example 
 from Mocks aren't stubs blog of Martin Fowler
 
 Mockito Features
 ---
 
 1) Stubbing
  - doReturn
  - doThrow
  - doAnswer
  - Mocking void methods
  - Mockito.anyString()
  - Mockito.eq()
  - Other Mockito argument matchers
  - Returns deep stubs *(only if needed)
 
 2) Verification
  - verify without times
  - verify with times
  - verify no more invocations
  - verify zero interactions
  
 3) Argument Captor
 4) https://github.com/mockito/mockito/wiki/How-to-write-good-tests
 5) http://wiki.c2.com/?AlanKayOnMessaging