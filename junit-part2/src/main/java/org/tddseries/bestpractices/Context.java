package org.tddseries.bestpractices;

public class Context {
  private static X x = new X();

  public static class X {
    private static Y y = new Y();

    public Y getY() {
      return y;
    }

    public static class Y {
      private static ZManager zmanager = new ZManager();

      public ZManager getZmanager() {
        return zmanager;
      }

      public static class ZManager {
        private boolean initialized;

        public void init() throws Exception {
          if (initialized) {
            throw new Exception("Already initialized");
          }
          initialized = true;
        }

        public void doSomething() {

        }
      }
    }
  }

  public static X getX() {
    return x;
  }
}
