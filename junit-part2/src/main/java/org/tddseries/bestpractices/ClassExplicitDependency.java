package org.tddseries.bestpractices;

import org.tddseries.bestpractices.Context.X.Y.ZManager;

public class ClassExplicitDependency {

  private final ZManager zManager;

  public ClassExplicitDependency(ZManager zManager) {
    // zManager instance being passed must be already initialized
    this.zManager = zManager;
  }

  public void doSomething() {
    // Directly using ZManager, as it expects an already initialized object
    // to be passed via constructor
    zManager.doSomething();
  }

  public static void main(String[] args) {

  }
}
