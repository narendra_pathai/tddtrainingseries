package org.tddseries.bestpractices;

public class ClassHidingDependency {

  public void doSomething() {
    try {
      Context.getX().getY().getZmanager().init();
      Context.getX().getY().getZmanager().doSomething();
    } catch (Exception e) {
      // No problem somebody else might have called init.
      // Eating exception - Yuck!!
    }
  }
}
