package org.tddseries.bestpractices;

/**
 * As the name suggests a Singleton pattern with reset option.
 *
 * This is never a good idea, should really really be used very sparsely and should
 * not be considered a good option. This weapon should only be employed when working with chronic
 * legacy code that cannot be changed in any way.
 */
public class ResettableSingleton {

  private static final ResettableSingleton INSTANCE = new ResettableSingleton();

  private int doneCount;

  private ResettableSingleton() {
  }

  public static ResettableSingleton getInstance() {
    return INSTANCE;
  }

  public void doSomething() {
    doSomePrivateThingy();
    doneCount++;
  }

  private void doSomePrivateThingy() {
    // some business logic goes here
  }

  public void reset() {
    doneCount = 0;
  }
}
