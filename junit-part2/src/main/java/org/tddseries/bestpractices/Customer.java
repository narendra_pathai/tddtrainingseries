package org.tddseries.bestpractices;

/**
 *
 */
public class Customer {

  CustomerStatus status = CustomerStatus.REGULAR;

  /*
   * The way to test promote method is not to expose the status field by making it public
   * or something. If customer status is a part of public API then its all right. But if
   * it is internal to Customer class then we should not expose it.
   *
   * Rather we should check for the side effect that calling promote method has on Customer
   * class. In this case the side effect is that preferred customer gets a discount of 50%.
   * So we should check for that.
   */
  public void promote() {
    this.status = CustomerStatus.PREFERRED;
  }

  public double getDisount() {
    return this.status == CustomerStatus.PREFERRED ? 0.5 : 0;
  }

  enum CustomerStatus {
    REGULAR,
    PREFERRED;
  }
}
