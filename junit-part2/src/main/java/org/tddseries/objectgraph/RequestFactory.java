package org.tddseries.objectgraph;

public class RequestFactory {

  private final Authenticator authenticator;
  private final UserRepository userRepository;
  private final CreditCardProcessor creditCardProcessor;

  public RequestFactory(Authenticator authenticator, UserRepository userRepository,
                        CreditCardProcessor creditCardProcessor) {
    this.authenticator = authenticator;
    this.userRepository = userRepository;
    this.creditCardProcessor = creditCardProcessor;
  }

  public AuthenticationPage build() {
    return new AuthenticationPage(authenticator,
        new ChargePage(creditCardProcessor, userRepository));
  }
}
