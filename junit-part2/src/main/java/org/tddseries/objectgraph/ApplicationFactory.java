package org.tddseries.objectgraph;

/**
 * Factories have the responsibility of wiring the objects together. This is where the bulk of new operators are located.
 * All objects created by application factory are singleton in scope.
 */
public class ApplicationFactory {
  private RequestFactory requestFactory;

  public void build() {
    Database database = new Database(); // leaf of graph has no dependency, so gets created first
    OfflineQueue offlineQueue = new OfflineQueue(database);
    Authenticator authenticator = new Authenticator(database);
    UserRepository userRepository = new UserRepository(database);
    CreditCardProcessor creditCardProcessor = new CreditCardProcessor(offlineQueue);
    requestFactory = new RequestFactory(authenticator, userRepository, creditCardProcessor);
  }

  public RequestFactory getRequestFactory() {
    return requestFactory;
  }
}
