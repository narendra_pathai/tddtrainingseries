package org.tddseries.random.good;

import java.util.Random;

public class DiceGame {
  private Random random;

  // Random injected via constructor so we can control the randomness by mocking Random from unit test cases
  DiceGame(Random random) {
    this.random = random;
  }

  /**
   * Rolls dice three times and returns result. Game can be won if dice value is equal to 6.
   */
  public boolean rollDice() {
    return isLucky()
            && isLucky()
            && isLucky();
  }

  private boolean isLucky() {
    return roll() == 6;
  }

  private int roll() {
    return random.ints(1, 7).findFirst().getAsInt();
  }
}

