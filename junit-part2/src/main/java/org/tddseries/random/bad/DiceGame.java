package org.tddseries.random.bad;

import java.util.Random;

public class DiceGame {
  private Random random = new Random();

  /**
   * Rolls dice three times and returns result. Game can be won if dice value is equal to 6.
   */
  public boolean rollDice() {
    return isLucky()
            && isLucky()
            && isLucky();
  }

  private boolean isLucky() {
    return roll() == 6;
  }

  private int roll() {
    return random.ints(1, 7).findFirst().getAsInt();
  }
}
