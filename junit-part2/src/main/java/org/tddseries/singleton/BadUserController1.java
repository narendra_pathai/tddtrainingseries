package org.tddseries.singleton;

import java.util.concurrent.TimeUnit;

/**
 * Anti practices in this class:
 * 1) Hardcoded dependency on {@link SNMPService} singleton, a big no no
 * 2) Constructor dependency is a concrete class and not an interface.
 * 3) Uses {@link System#nanoTime()} directly which is external dependency because it is not in control
 * of test cases.
 */
public class BadUserController1 {

  private final HibernateUserRepository userRepository;

  BadUserController1(HibernateUserRepository userRepository) {
    this.userRepository = userRepository;
  }

  public void processUserRequest(UserRequest request) {
    long start = System.nanoTime();
    User user = userRepository.getUser(request.getUserId());
    long end = System.nanoTime();
    if (TimeUnit.MILLISECONDS.toSeconds(end - start) > 1) {
      SNMPService.getInstance().generateAlert("User lookup operation took more than 1 sec");
    }
    processUser(user);
  }

  private void processUser(User user) {
    System.out.println(user.getFirstName() + " profile found");
  }
}
