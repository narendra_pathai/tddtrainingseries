package org.tddseries.singleton;

import javax.annotation.Nonnull;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Good practices in this class:
 * 1) Constructor explicitly states all dependencies and expects interfaces not concrete classes.
 */
public class GoodUserController {

  private final UserRepository userRepository;
  private final AlertService alertService;
  private final Ticker ticker;

  public GoodUserController(@Nonnull UserRepository userRepository, @Nonnull AlertService alertService,
                            @Nonnull Ticker ticker) {
    this.userRepository = Objects.requireNonNull(userRepository);
    this.alertService = Objects.requireNonNull(alertService);
    this.ticker = Objects.requireNonNull(ticker);
  }

  public void processUserRequest(UserRequest request) {
    long start = ticker.nanoTime();
    try {
      User user = userRepository.getUser(request.getUserId());
      long end = ticker.nanoTime();
      if (TimeUnit.NANOSECONDS.toSeconds(end - start) > 1) {
        alertService.generateAlert("User lookup operation took more than 1 sec");
      }
      processUser(user);
    } catch (OperationFailedException ex) {
      alertService.generateAlert("Error in locating user from repository");
    }
  }

  // Dummy processing of user
  private void processUser(User user) {
    System.out.println(user.getFirstName() + " profile found");
  }
}
