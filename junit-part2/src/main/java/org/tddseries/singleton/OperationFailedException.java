package org.tddseries.singleton;

public class OperationFailedException extends Exception {

  OperationFailedException(String message, Throwable cause) {
    super(message, cause);
  }
}
