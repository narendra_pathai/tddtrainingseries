package org.tddseries.singleton;


public interface UserRepository {

  /**
   * @throws OperationFailedException if datasource connectivity is lost or timeout
   */
  User getUser(String userId) throws OperationFailedException;
}
