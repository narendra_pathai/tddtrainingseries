package org.tddseries.singleton;

/**
 * An abstraction over {@link System#nanoTime()} external dependency. This nifty abstraction is useful for making
 * unit test cases predictable.
 */
public interface Ticker {
  long nanoTime();

  /**
   * Returns an instance of System ticker, which uses {@link System#nanoTime()}
   */
  static Ticker systemTicker() {
    return System::nanoTime;
  }
}
