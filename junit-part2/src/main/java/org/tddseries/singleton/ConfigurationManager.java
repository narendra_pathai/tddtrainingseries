package org.tddseries.singleton;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class ConfigurationManager {
  public static final String SNMP = "SNMP";

  private static volatile ConfigurationManager instance;
  private Map<String, Object> configurationMap;

  private ConfigurationManager() {
  }

  public static ConfigurationManager getInstance() {
    if (instance == null) {
      synchronized (ConfigurationManager.class) {
        if (instance == null) {
          instance = new ConfigurationManager();
        }
      }
    }
    return instance;
  }

  public void init() {
    configurationMap = new HashMap<>();
    try {
      readSNMPConfiguration();
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  private void readSNMPConfiguration() throws IOException {
    InputStream fis = ConfigurationManager.class.getClassLoader().getResourceAsStream("snmp-service.xml");
    if (fis == null) {
      throw new FileNotFoundException("SNMP service configuration not found");
    }
    try {
      configurationMap.put(SNMP, new Object());
    } finally {
      fis.close();
    }
  }

  public Object get(String key) {
    return configurationMap.get(key);
  }
}
