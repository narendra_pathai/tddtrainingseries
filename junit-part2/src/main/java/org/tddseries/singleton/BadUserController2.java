package org.tddseries.singleton;

import java.util.concurrent.TimeUnit;

/**
 * Anti practices in this class:
 * 1) Constructor dependencies {@link HibernateUserRepository} and {@link SNMPService} are concrete classes and not interfaces.
 *
 * Good practices in this class:
 * 1) Current time {@link System#nanoTime()} external dependency is abstracted and asked via constructor
 */
public class BadUserController2 {

  private final HibernateUserRepository userRepository;
  private final SNMPService snmpService;
  private final Ticker ticker;

  /**
   * Good that constructor explicitly states it's dependencies and doesn't out of the box use a singleton class.
   * Bad that constructor asks for concrete classes and not interface
   */
  BadUserController2(HibernateUserRepository userRepository, SNMPService snmpService) {
    this(userRepository, snmpService, Ticker.systemTicker());
  }

  BadUserController2(HibernateUserRepository userRepository, SNMPService snmpService, Ticker ticker) {
    this.userRepository = userRepository;
    this.snmpService = snmpService;
    this.ticker = ticker;
  }

  public void processUserRequest(UserRequest request) {
    long start = ticker.nanoTime();
    User user = userRepository.getUser(request.getUserId());
    long end = ticker.nanoTime();
    if (TimeUnit.NANOSECONDS.toSeconds(end - start) > 1) {
      snmpService.generateAlert("User lookup operation took more than 1 sec");
    }
    processUser(user);
  }

  private void processUser(User user) {
    System.out.println(user.getFirstName() + " profile found");
  }
}
