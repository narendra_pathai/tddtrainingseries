package org.tddseries.singleton;

import java.io.IOException;
import java.net.Socket;

public class SNMPService {

  private static volatile SNMPService instance;
  private Object snmpConfiguration;
  private Socket socket;

  private SNMPService() {
  }

  public void generateAlert(String message) {
    try {
      socket.getOutputStream().write(message.getBytes());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static SNMPService getInstance() {
    if (instance == null) {
      synchronized (SNMPService.class) {
        if (instance == null) {
          instance = new SNMPService();
        }
      }
    }
    return instance;
  }

  public void init() {
    readSystemConfiguration();
    initializeConnectionToSNMPServer();
  }

  private void initializeConnectionToSNMPServer() {
    socket = new Socket();
    throw new RuntimeException("SNMP service connectivity not allowed in unit test cases");
  }

  private void readSystemConfiguration() {
    this.snmpConfiguration = ConfigurationManager.getInstance().get(ConfigurationManager.SNMP);
  }
}
