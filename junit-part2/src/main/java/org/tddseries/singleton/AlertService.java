package org.tddseries.singleton;

public interface AlertService {
  void generateAlert(String message);
}
