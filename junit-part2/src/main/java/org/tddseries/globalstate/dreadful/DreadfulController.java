package org.tddseries.globalstate.dreadful;

/**
 * Anti practices in this class:
 * 1) Creates instance of dependency itself in constructor or init
 */
public class DreadfulController {

  private DreadfulDependency dreadfulDependency;

  DreadfulController() {
    dreadfulDependency = new DreadfulDependency();
  }

  public void doSomething() {
    dreadfulDependency.doSomething();
  }
}
