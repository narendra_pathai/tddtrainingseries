package org.tddseries.globalstate.dreadful;

/**
 * Anti practices in this class:
 * 1) {@link DreadfulDependency#doSomething()} makes static call to a class that holds state. This causes issues when
 * trying to unit test. Refer unit tests for {@link DreadfulController}.
 *
 * IMPORTANT NOTE: It is okay to make static calls to utilities which don't hold any state and just return the result.
 * For example its okay to call {@link Math#abs(int)} or similar utilities because they don't have any state.
 */
public class DreadfulDependency {

  public void doSomething() {
    DreadfulHelper.doDreadfulStuff();
  }
}
