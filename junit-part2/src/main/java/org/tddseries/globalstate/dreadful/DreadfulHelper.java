package org.tddseries.globalstate.dreadful;

/**
 * Anti practices in this class:
 * 1) Decides by itself that it wants to be a global by having static variable. Doing this does not allow us to unit
 * test any class that is dependent on it predictably. See test cases for {@link DreadfulController}.
 */
public class DreadfulHelper {
  private static int globalEvilState = 0;

  public static void doDreadfulStuff() {
    if (++globalEvilState % 2 == 0) {
      throw new RuntimeException("Global evil hidden state will haunt you forever!");
    }
  }
}
