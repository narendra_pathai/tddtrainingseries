package org.tddseries.globalstate.pleasant;

public class PleasantControllerMain {

  public static void main(String[] args) {
    PleasantHelper globalHelper = new PleasantHelper();
    PleasantDependency pleasantDependency = new PleasantDependency(globalHelper);

    PleasantController controller1 = new PleasantController(pleasantDependency);
    PleasantController controller2 = new PleasantController(pleasantDependency);

    controller1.doSomething();
    // will throw exception because of shared PleasantHelper
    controller2.doSomething();
  }
}
