package org.tddseries.globalstate.pleasant;

public class PleasantDependency {

  private final PleasantHelper helper;

  PleasantDependency(PleasantHelper helper) {
    this.helper = helper;
  }

  public void doSomething() {
    helper.doPleasantStuff();
  }
}
