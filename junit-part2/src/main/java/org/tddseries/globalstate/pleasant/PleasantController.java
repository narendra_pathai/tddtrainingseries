package org.tddseries.globalstate.pleasant;

public class PleasantController {

  private final PleasantDependency pleasantDependency;

  PleasantController(PleasantDependency pleasantDependency) {
    this.pleasantDependency = pleasantDependency;
  }

  public void doSomething() {
    pleasantDependency.doSomething();
  }
}
