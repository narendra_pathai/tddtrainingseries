package org.tddseries.globalstate.pleasant;

import org.tddseries.globalstate.dreadful.DreadfulHelper;

/**
 * {@link PleasantHelper} is better than {@link DreadfulHelper} because it doesn't decide whether its state should be
 * global (static) or not. It leaves that on to the dependency injection mechanism to ensure that single object of
 * helper is shared in whole application, not via static variable but by passing same object anywhere where helper is
 * needed.
 */
public class PleasantHelper {
  private int nonGlobalState = 0;

  public void doPleasantStuff() {
    if (++nonGlobalState % 2 == 0) {
      throw new RuntimeException("Don't like evens much!");
    }
  }
}
