package org.tddseries.mockito;

import org.tddseries.singleton.SNMPService;

import java.util.Observable;
import java.util.Observer;

/**
 * Generates SNMP alert whenever change in weather is observed
 */
public class WeatherObserver implements Observer {

  @Override
  public void update(Observable o, Object arg) {
    SNMPService.getInstance().generateAlert("Current weather is : " + arg);
  }
}
