package org.tddseries.mockito;

import java.util.Observable;

public class WeatherObservable extends Observable {

    private String weather;

    void setWeather(String weather) {
      this.weather = weather;
      setChanged();
      notifyObservers(weather);
    }
}
