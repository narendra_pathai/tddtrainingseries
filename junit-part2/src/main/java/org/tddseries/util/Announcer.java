package org.tddseries.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.EventListener;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * A dynamic proxy based observer. Traditionally when we need listener mechanism we use java {@link java.util.Observer} class. But
 * it has limitation that you need to extent that class. Suppose you already are extending a class then you cannot support both.
 *
 * This class converts that pattern from extension(IS-A) to composition(HAS-A) principle. Whenever a class needs to become a observer
 * then it needs to create an instance of {@link Announcer} and keep that instance. The listeners will be registered to the announcer
 * instance and any event fired on announcer will be observed by all the listeners.
 *
 * @param <T> type of listener
 */
public class Announcer<T extends EventListener> {

  private T proxy;
  private CopyOnWriteArrayList<T> listeners = new CopyOnWriteArrayList<>();

  @SuppressWarnings("unchecked")
  public Announcer(Class<T> clazz) {
    this.proxy = createProxy(clazz);
  }

  private T createProxy(Class<T> clazz) {
    return clazz.cast(Proxy.newProxyInstance(clazz.getClassLoader(), new Class[] {clazz}, (invokedObject, method, args) -> {
      announce(method, args);
      return null;
    }));
  }

  private void announce(Method method, Object[] args) throws InvocationTargetException, IllegalAccessException {
    for (T listener : listeners) {
      method.invoke(listener, args);
    }
  }

  public static <T extends EventListener> Announcer<T> to(Class<T> clazz) {
    return new Announcer<>(clazz);
  }

  public void addListener(T listener) {
    this.listeners.add(Objects.requireNonNull(listener));
  }

  public void removeListener(T listener) {
    this.listeners.remove(listener);
  }

  public T announce() {
    return proxy;
  }
}
