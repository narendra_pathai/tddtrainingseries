package org.tddseries.util;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class DefaultTaskScheduler implements TaskScheduler {
  private ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);

  @Override
  public void scheduleFixedDelayTask(Runnable command, long delay, TimeUnit unit) {
    executorService.scheduleWithFixedDelay(command, delay, delay, unit);
  }

  public void shutdown() throws InterruptedException {
    executorService.shutdown();
    executorService.awaitTermination(10, TimeUnit.SECONDS);
    executorService.shutdownNow();
  }
}
