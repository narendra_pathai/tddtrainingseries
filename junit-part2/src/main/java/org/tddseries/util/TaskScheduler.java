package org.tddseries.util;

import java.util.concurrent.TimeUnit;

public interface TaskScheduler {
  void scheduleFixedDelayTask(Runnable command, long delay, TimeUnit unit);
}
