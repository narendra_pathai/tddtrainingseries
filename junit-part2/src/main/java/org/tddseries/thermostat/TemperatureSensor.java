package org.tddseries.thermostat;

public interface TemperatureSensor {
  double getReading();
}
