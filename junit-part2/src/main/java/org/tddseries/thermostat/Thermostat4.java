package org.tddseries.thermostat;

import org.tddseries.util.Announcer;
import org.tddseries.util.TaskScheduler;

import java.util.concurrent.TimeUnit;

public class Thermostat4 {
  private final TemperatureSensor sensor;
  private final Announcer<TemperatureListener> announcer;
  private double targetTemperature;
  private volatile double currentTemperature;

  public Thermostat4(double targetTemperature, TemperatureSensor sensor, TaskScheduler taskScheduler) {
    this.targetTemperature = targetTemperature;
    this.sensor = sensor;
    this.announcer = Announcer.to(TemperatureListener.class);
    taskScheduler.scheduleFixedDelayTask(() -> {
        setCurrentTemperatureReading(sensor.getReading());
    }, 1, TimeUnit.SECONDS);
  }

  public void addListener(TemperatureListener listener) {
    this.announcer.addListener(listener);
  }

  public void removeListener(TemperatureListener listener) {
    this.announcer.removeListener(listener);
  }

  public void stop() throws InterruptedException {
    // TODO cancel the future
  }

  private void setCurrentTemperatureReading(double temperature) {
    this.currentTemperature = temperature;
    generateAlert(temperature);
  }

  private void generateAlert(double temperature) {
    if (tooCold(temperature)) {
      announcer.announce().onTemperatureTooLow(temperature);
    } else if (tooHot(temperature)) {
      announcer.announce().onTemperatureTooHigh(temperature);
    } else {
      // targetTemperature is just fine
    }
  }

  public double getCurrentTemperature() {
    return currentTemperature;
  }

  private boolean tooHot(double temperature) {
    return temperature - this.targetTemperature > 0.1;
  }

  private boolean tooCold(double temperature) {
    return temperature - this.targetTemperature < -0.1;
  }
}
