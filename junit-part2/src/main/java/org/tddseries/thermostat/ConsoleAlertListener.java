package org.tddseries.thermostat;

public class ConsoleAlertListener implements TemperatureListener {

  @Override
  public void onTemperatureTooHigh(double temperature) {
    System.out.println(String.format("Temperature %f is too high", temperature));
  }

  @Override
  public void onTemperatureTooLow(double temperature) {
    System.out.println(String.format("Temperature %f is too low", temperature));
  }
}
