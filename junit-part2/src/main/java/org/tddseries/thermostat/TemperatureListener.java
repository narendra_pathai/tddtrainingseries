package org.tddseries.thermostat;

import java.util.EventListener;

public interface TemperatureListener extends EventListener {
  void onTemperatureTooHigh(double temperature);
  void onTemperatureTooLow(double temperature);
}
