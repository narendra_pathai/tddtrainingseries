package org.tddseries.thermostat;

import org.tddseries.util.DefaultTaskScheduler;

public class ThermostatMain {

  public static void main(String[] args) throws InterruptedException {
    DefaultTaskScheduler defaultTaskScheduler = new DefaultTaskScheduler();
    Thermostat4 thermostat = new Thermostat4(24,
        new Chip121TemperatureSensor(24, RandomProvider.newDefaultProvider()),
        defaultTaskScheduler);
    thermostat.addListener(new ConsoleAlertListener());
    thermostat.addListener(new HeaterController());
    thermostat.addListener(new CompressorController());

    Thread.sleep(10000);

    defaultTaskScheduler.shutdown();
  }

  private static class CompressorController implements TemperatureListener {

    @Override
    public void onTemperatureTooHigh(double temperature) {
      System.out.println(String.format("Temperature: [%f] Compressor started", temperature));
    }

    @Override
    public void onTemperatureTooLow(double temperature) {
      System.out.println(String.format("Temperature: [%f] Compressor stopped", temperature));
    }
  }

  private static class HeaterController implements TemperatureListener {

    @Override
    public void onTemperatureTooHigh(double temperature) {
      System.out.println(String.format("Temperature: [%f] Heater stopped", temperature));
    }

    @Override
    public void onTemperatureTooLow(double temperature) {
      System.out.println(String.format("Temperature: [%f] Heater started", temperature));
    }
  }

  private static class XYZTemperatureSensor implements TemperatureSensor {

    @Override
    public double getReading() {
      // Use XYZ hardware to sense temperature
      return 0;
    }
  }
}
