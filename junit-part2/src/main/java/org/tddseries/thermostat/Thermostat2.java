package org.tddseries.thermostat;

/**
 * Anti Practices in this class:
 * 1) Thermostat takes decision of which alert mechanism will be used which makes code tightly
 * coupled
 * 2) Raw threads are used which takes away control from when and how frequently temperature is sensed, again a problem
 * when doing unit testing.
 */
public class Thermostat2 {

  private final TemperatureSensor sensor;
  private final Alert alert;
  private double targetTemperature;
  private volatile double currentTemperature;
  private Thread sensorThread;

  public Thermostat2(double targetTemperature, TemperatureSensor sensor) {
    this.targetTemperature = targetTemperature;
    this.sensor = sensor;
    this.alert = new Alert();
    // Creates thread which makes it difficult to test
    sensorThread = new Thread(() -> {
      while (!Thread.interrupted()) {
        sleep();
        setCurrentTemperatureReading(sensor.getReading());
      }
    });
    sensorThread.start();
  }

  private void setCurrentTemperatureReading(double temperature) {
    this.currentTemperature = temperature;
    generateAlert(temperature);
  }

  private void generateAlert(double temperature) {
    if (tooCold(temperature)) {
      alert.temperatureTooLow(temperature);
    } else if (tooHot(temperature)) {
      alert.temperatureTooHigh(temperature);
    } else {
      // targetTemperature is just fine
    }
  }

  public double getCurrentTemperature() {
    return currentTemperature;
  }

  private void sleep() {
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
  }

  private boolean tooHot(double temperature) {
    return temperature - this.targetTemperature > 0.1;
  }

  private boolean tooCold(double temperature) {
    return temperature - this.targetTemperature < -0.1;
  }

  public void stop() throws InterruptedException {
    sensorThread.interrupt();
    sensorThread.join();
  }


  class Alert {
    void temperatureTooHigh(double temperature) {
      System.out.println(String.format("Temperature %f is too high", temperature));
    }

    void temperatureTooLow(double temperature) {
      System.out.println(String.format("Temperature %f is too low", temperature));
    }
  }
}
