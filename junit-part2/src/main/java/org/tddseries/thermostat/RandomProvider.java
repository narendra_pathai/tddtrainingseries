package org.tddseries.thermostat;

import java.util.Random;

public interface RandomProvider {
  double nextDouble(double from, double to);

  /**
   * Returns a new instance of default random provider that uses {@link Random} to
   * generate random.
   *
   * @return a new instance of default random provider
   */
  static RandomProvider newDefaultProvider() {
    Random random = new Random(System.currentTimeMillis());
    return (from, to) -> random.doubles(from, to).findFirst().getAsDouble();
  }
}
