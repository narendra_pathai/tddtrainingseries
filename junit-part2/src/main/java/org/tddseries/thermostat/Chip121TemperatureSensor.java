package org.tddseries.thermostat;

public class Chip121TemperatureSensor implements TemperatureSensor {

  private final double targetTemperature;
  private final RandomProvider randomProvider;

  Chip121TemperatureSensor(double targetTemperature, RandomProvider randomProvider) {
    this.targetTemperature = targetTemperature;
    this.randomProvider = randomProvider;
  }

  @Override
  public double getReading() {
    return randomProvider.nextDouble(targetTemperature - 1.0, targetTemperature + 1.0);
  }
}
