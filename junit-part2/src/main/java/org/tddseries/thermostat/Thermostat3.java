package org.tddseries.thermostat;

import org.tddseries.util.Announcer;

/**
 * Anti Practices in this class:
 * 1) Raw threads are used which takes away control from when and how frequently temperature is sensed, again a problem
 * when doing unit testing.
 */
public class Thermostat3 {
  private final TemperatureSensor sensor;
  private final Announcer<TemperatureListener> announcer;
  private double targetTemperature;
  private volatile double currentTemperature;
  private Thread sensorThread;

  public Thermostat3(double targetTemperature, TemperatureSensor sensor) {
    this.targetTemperature = targetTemperature;
    this.sensor = sensor;
    this.announcer = Announcer.to(TemperatureListener.class);
    // Creates thread which makes it difficult to test
    sensorThread = new Thread(() -> {
      while (!Thread.interrupted()) {
        sleep();
        setCurrentTemperatureReading(sensor.getReading());
      }
    });
    sensorThread.start();
  }

  public void addListener(TemperatureListener listener) {
    this.announcer.addListener(listener);
  }

  public void removeListener(TemperatureListener listener) {
    this.announcer.removeListener(listener);
  }

  public void stop() throws InterruptedException {
    sensorThread.interrupt();
    sensorThread.join();
  }

  private void setCurrentTemperatureReading(double temperature) {
    this.currentTemperature = temperature;
    generateAlert(temperature);
  }

  private void generateAlert(double temperature) {
    if (tooCold(temperature)) {
      announcer.announce().onTemperatureTooLow(temperature);
    } else if (tooHot(temperature)) {
      announcer.announce().onTemperatureTooHigh(temperature);
    } else {
      // targetTemperature is just fine
    }
  }

  public double getCurrentTemperature() {
    return currentTemperature;
  }

  private void sleep() {
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
  }

  private boolean tooHot(double temperature) {
    return temperature - this.targetTemperature > 0.1;
  }

  private boolean tooCold(double temperature) {
    return temperature - this.targetTemperature < -0.1;
  }
}
