package org.tddseries.localdate;

import java.time.LocalDate;
import java.time.Month;

public class DiwaliDayProvider {

  public LocalDate get(int year) {
    // Use hindu calendar to determine the exact date and time, for now it occurs on 21st of october every year
    return LocalDate.of(year, Month.OCTOBER, 21);
  }
}
