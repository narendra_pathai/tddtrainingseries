package org.tddseries.localdate;

import java.time.LocalDate;

public class DiwaliPromotionFactory1 {

  private final DiwaliDayProvider diwaliDayProvider;

  public DiwaliPromotionFactory1(DiwaliDayProvider diwaliDayProvider) {
    this.diwaliDayProvider = diwaliDayProvider;
  }

  public DiwaliPromotion get() {
    LocalDate now = LocalDate.now();
    // Calculate which day will be Diwali this year
    LocalDate diwali = diwaliDayProvider.get(now.getYear());
    return new DiwaliPromotion(diwali.atStartOfDay());
  }
}
