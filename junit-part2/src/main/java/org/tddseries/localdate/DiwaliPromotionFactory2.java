package org.tddseries.localdate;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class DiwaliPromotionFactory2 {
  private final DiwaliDayProvider diwaliDayProvider;
  private final Clock clock;

  public DiwaliPromotionFactory2(DiwaliDayProvider diwaliDayProvider, Clock clock) {
    this.diwaliDayProvider = diwaliDayProvider;
    this.clock = clock;
  }

  public DiwaliPromotion get() {
    LocalDateTime now = LocalDateTime.now(clock);
    // Calculate which day will be Diwali this year
    LocalDate diwali = diwaliDayProvider.get(now.getYear());
    return new DiwaliPromotion(diwali.atStartOfDay());
  }
}
