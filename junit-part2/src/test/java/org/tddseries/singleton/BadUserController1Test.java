package org.tddseries.singleton;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;
import org.tddseries.singleton.BadUserController1;
import org.tddseries.singleton.ConfigurationManager;
import org.tddseries.singleton.HibernateUserRepository;
import org.tddseries.singleton.SNMPService;
import org.tddseries.singleton.User;
import org.tddseries.singleton.UserRequest;

/**
 * Demonstrates the following:
 * 1) HOW NOT TO WRITE TEST CASES
 * 2) HOW DESIGN SHOULD NOT LOOK LIKE
 * 3) Using {@link Mockito#doAnswer(Answer)} for stubbing behavior
 * 4) Using @{@link Mock} annotation for mocking objects
 */
public class BadUserController1Test {

  private static final String TEST_USER_NAME = "TEST_USER";
  private BadUserController1 controller;

  /**
   * Should not use spy here because spy does not provide mocking by default it calls real
   * implementation and provides verification. When using spy we will have to mock calls as well.
   */
  @Mock
  private HibernateUserRepository mockHibernateRepository;

  @Before
  public void createController() {
    MockitoAnnotations.initMocks(this);
    controller = new BadUserController1(mockHibernateRepository);
  }

  @Test
  public void generatesAlertIfProfileLocationTakesMoreThanASecond() {
    UserRequest userRequest = newUserRequest();
    User user = new User();
    user.setFirstName(TEST_USER_NAME);
    user.setId(userRequest.getUserId());

    Mockito.doAnswer(invocationOnMock -> {
      Thread.sleep(2000);
      return user;
    }).when(mockHibernateRepository).getUser(Mockito.anyString());

    ConfigurationManager.getInstance().init();
    // Tries to establish connection to SNMP service
    SNMPService.getInstance().init();
    controller.processUserRequest(userRequest);
    // Can't really test this now because its tightly coupled with SNMP
  }

  private UserRequest newUserRequest() {
    UserRequest userRequest = new UserRequest();
    userRequest.setUserId("1");
    userRequest.setBody(new Object());
    return userRequest;
  }
}