package org.tddseries.singleton;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;
import org.tddseries.singleton.BadUserController2;
import org.tddseries.singleton.HibernateUserRepository;
import org.tddseries.singleton.SNMPService;
import org.tddseries.singleton.Ticker;
import org.tddseries.singleton.User;
import org.tddseries.singleton.UserRequest;

import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Demonstrates the following:
 * 1) Using {@link Mockito#doAnswer(Answer)} for stubbing behavior
 * 2) Using @{@link Mock} annotation for mocking objects
 * 3) Using {@link Mockito#doReturn(Object)} for returning a object when method is invoked on mock
 */
public class BadUserController2Test {

  private static final String TEST_USER_NAME = "TEST_USER";
  private BadUserController2 controller;

  /**
   * Should not use spy here because spy does not provide mocking by default it calls real
   * implementation and provides verification. When using spy we will have to mock calls as well.
   */
  @Mock
  private HibernateUserRepository mockHibernateRepository;

  /**
   * We are mocking concrete classes here but that is not a good practice to follow. Reason is if the class is final
   * then mocking will fail.
   */
  @Mock
  private SNMPService mockSnmpService;
  private FixedTicker fixedTicker;

  @Before
  public void createController() {
    MockitoAnnotations.initMocks(this);
    fixedTicker = new FixedTicker(Arrays.asList(0L, 0L));
    controller = new BadUserController2(mockHibernateRepository, mockSnmpService, fixedTicker);
  }

  @Test
  public void generatesAlertIfProfileLocationTakesMoreThanASecond() {
    UserRequest userRequest = newUserRequest();
    User user = creatUser(userRequest);

    doReturn(user).when(mockHibernateRepository).getUser(user.getId());
    when(mockHibernateRepository.getUser(Mockito.anyString())).thenReturn(user);

    fixedTicker.setSource(Arrays.asList(0L, TimeUnit.SECONDS.toNanos(2)));

    controller.processUserRequest(userRequest);

    verify(mockSnmpService).generateAlert("User lookup operation took more than 1 sec");
  }

  private User creatUser(UserRequest userRequest) {
    User user = new User();
    user.setFirstName(TEST_USER_NAME);
    user.setId(userRequest.getUserId());
    return user;
  }

  private UserRequest newUserRequest() {
    UserRequest userRequest = new UserRequest();
    userRequest.setUserId("1");
    userRequest.setBody(new Object());
    return userRequest;
  }

  private class FixedTicker implements Ticker {
    private Iterator<Long> source;

    FixedTicker(Iterable<Long> source) {
      this.source = source.iterator();
    }

    public void setSource(Iterable<Long> source) {
      this.source = source.iterator();
    }

    @Override
    public long nanoTime() {
      return source.next();
    }
  }
}