package org.tddseries.singleton;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.tddseries.singleton.AlertService;
import org.tddseries.singleton.GoodUserController;
import org.tddseries.singleton.OperationFailedException;
import org.tddseries.singleton.Ticker;
import org.tddseries.singleton.User;
import org.tddseries.singleton.UserRepository;
import org.tddseries.singleton.UserRequest;

import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Demonstates the following:
 * 1) Interfaces are mocked not concrete classes
 * 2) mock exception situation {@link Mockito#doThrow(Throwable)}
 */
public class GoodUserControllerTest {

  private static final String TEST_USER_NAME = "TEST_USER";
  private GoodUserController controller;

  /**
   * Mocking the interface, which is a good practice
   */
  @Mock
  private UserRepository mockUserRepository;

  /**
   * Mocking the interface, which is a good practice
   */
  @Mock
  private AlertService mockAlertService;
  private FixedTicker fixedTicker;

  @Before
  public void createController() {
    MockitoAnnotations.initMocks(this);
    fixedTicker = new FixedTicker(Arrays.asList(0L, 0L));
    controller = new GoodUserController(mockUserRepository, mockAlertService, fixedTicker);
  }

  @Test
  public void generatesAlertIfProfileLookupTakesMoreThanASecond() throws OperationFailedException {
    UserRequest userRequest = newUserRequest();
    User user = creatUser(userRequest);

    doReturn(user).when(mockUserRepository).getUser(user.getId());
    when(mockUserRepository.getUser(Mockito.anyString())).thenReturn(user);

    fixedTicker.setSource(Arrays.asList(0L, TimeUnit.SECONDS.toNanos(2)));

    controller.processUserRequest(userRequest);

    verify(mockAlertService).generateAlert("User lookup operation took more than 1 sec");
  }

  @Test
  public void generatesAlertIfProfileLookupFails() throws OperationFailedException {
    UserRequest userRequest = newUserRequest();
    User user = creatUser(userRequest);

    doThrow(OperationFailedException.class).when(mockUserRepository).getUser(user.getId());

    fixedTicker.setSource(Arrays.asList(0L, TimeUnit.SECONDS.toNanos(2)));

    controller.processUserRequest(userRequest);

    verify(mockAlertService).generateAlert("Error in locating user from repository");
  }

  private User creatUser(UserRequest userRequest) {
    User user = new User();
    user.setFirstName(TEST_USER_NAME);
    user.setId(userRequest.getUserId());
    return user;
  }

  private UserRequest newUserRequest() {
    UserRequest userRequest = new UserRequest();
    userRequest.setUserId("1");
    userRequest.setBody(new Object());
    return userRequest;
  }

  private class FixedTicker implements Ticker {
    private Iterator<Long> source;

    FixedTicker(Iterable<Long> source) {
      this.source = source.iterator();
    }

    public void setSource(Iterable<Long> source) {
      this.source = source.iterator();
    }

    @Override
    public long nanoTime() {
      return source.next();
    }
  }
}
