package org.tddseries.util;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.tddseries.util.Announcer;

import java.util.EventListener;

/**
 * Demonstrates the use of following:
 *  1) Mock
 *  2) Verification
 *  3) In order verification
 *  4) Verify zero invocation
 */
public class AnnouncerTest {

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  /**
   * Mocks are being used here not to stub the behavior but for verification purposes.
   */
  @Mock
  private CustomListener listener1;

  @Mock
  private CustomListener listener2;

  private Announcer<CustomListener> announcer;

  @Before
  public void initMocks() {
    MockitoAnnotations.initMocks(this);
    createAnnouncer();
  }

  private void createAnnouncer() {
    announcer = Announcer.to(CustomListener.class);
    announcer.addListener(listener1);
    announcer.addListener(listener2);
  }

  @Test
  public void announcesEventToRegisteredListenersInOrderOfAddition() {
    // Act
    announcer.announce().onCustomEvent();

    // Assert
    InOrder inOrder = Mockito.inOrder(listener1, listener2); // the order in which objects are added here doesn't matter
    inOrder.verify(listener1).onCustomEvent();
    inOrder.verify(listener2).onCustomEvent();
  }

  @Test
  public void announcesEventWithArgToRegisteredListenersInOrderOfAddition() {
    // Act
    String arg = "Custom Argument";
    announcer.announce().onCustomEventWithArg(arg);

    // Assert
    InOrder inOrder = Mockito.inOrder(listener1, listener2); // the order in which objects are added here doesn't matter
    inOrder.verify(listener1).onCustomEventWithArg(arg);
    inOrder.verify(listener2).onCustomEventWithArg(arg);
  }

  @Test
  public void doesNotAnnounceEventsToRemovedListeners() {
    announcer.removeListener(listener1);

    announcer.announce().onCustomEvent();

    verify(listener2).onCustomEvent();
    verifyZeroInteractions(listener1);
  }

  /**
   * API tests
   */
  @Test
  public void doesNotAllowAddingNullListener() {
    thrown.expect(NullPointerException.class);

    announcer.addListener(null);
  }

  private interface CustomListener extends EventListener {
    void onCustomEvent();
    void onCustomEventWithArg(Object arg);
  }
}
