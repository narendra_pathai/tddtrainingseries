package org.tddseries.random.good;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.OptionalInt;
import java.util.Random;

import static java.util.OptionalInt.of;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(JUnitParamsRunner.class)
public class DiceGameTest {

  @Rule
  public MockitoRule mockito = MockitoJUnit.rule();

  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private Random mockRandom;

  @InjectMocks
  private DiceGame game;

  @Test
  public void gameIsWonIfThreeConsecutiveRollsOfDiceFaceShows6() {

    Mockito.when(mockRandom.ints(Mockito.anyInt(), Mockito.anyInt())
        .findFirst()).thenReturn(of(6));

    boolean result = game.rollDice();

    assertTrue(result);
    verify(mockRandom.ints(1, 7),
            times(3)).findFirst();
  }

  @Test
  @Parameters({"1,2,6", "6,6,1", "6,1,6"})
  public void gameIsLostIfDiceFaceShowsValueGreaterLessThanOrEqualToThree(int rollFirst, int rollTwo, int rollThree) {
    Mockito.when(mockRandom.ints(Mockito.anyInt(), Mockito.anyInt())
        .findFirst()).thenReturn(of(rollFirst), of(rollTwo), of(rollThree));

    boolean result = game.rollDice();

    Assert.assertFalse(result);
  }

}
