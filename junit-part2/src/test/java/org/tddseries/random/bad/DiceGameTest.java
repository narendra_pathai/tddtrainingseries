package org.tddseries.random.bad;

import org.junit.Test;

public class DiceGameTest {

  private DiceGame game = new DiceGame();

  /**
   * Surely enough I do get coverage when testing, but nothing is being asserted in this test case because of
   * unpredictability.
   */
  @Test
  public void testSomethingHappens() {
    boolean unpredictableResult = game.rollDice();

    // We cannot predict whether game is working or not because it is using random value and we cannot control it.
    System.out.println(unpredictableResult);
  }
}
