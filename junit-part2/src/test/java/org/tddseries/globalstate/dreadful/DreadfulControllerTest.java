package org.tddseries.globalstate.dreadful;

import org.junit.Before;
import org.junit.Test;
import org.tddseries.globalstate.dreadful.DreadfulController;

/**
 * DreadfulController test shows hidden global state problem. Even though we think we are creating two different instances
 * of controller which will be isolated from each other and will not share any state across tests, there is hidden global
 * state. static variable is global, and due to that there is hidden sharing of state
 * between the two instances of controller.
 *
 * Hence when we run single test case they will always pass, but as soon as we run both together, any of them can fail.
 */
public class DreadfulControllerTest {

  private DreadfulController controller;

  @Before
  public void createController() {
    controller = new DreadfulController();
  }

  /**
   * This test case will pass if only this test case is run but if it is run with other test case, it may fail
   * because of hidden global state
   */
  @Test
  public void thisTestMayFail() {
    controller.doSomething();
  }

  /**
   * This test case will pass if only this test case is run but if it is run with other test case, it may fail
   * because of hidden global state
   */
  @Test
  public void thisTestMayFailToo() {
    controller.doSomething();
  }
}
