package org.tddseries.globalstate.pleasant;

import org.junit.Before;
import org.junit.Test;
import org.tddseries.globalstate.pleasant.PleasantController;
import org.tddseries.globalstate.pleasant.PleasantDependency;
import org.tddseries.globalstate.pleasant.PleasantHelper;

public class PleasantControllerTest {

  private PleasantController controller;

  @Before
  public void createController() {
    /*
     * This is what inversion of control means, the object creation graph is inverted.
     * In DreadfulControllerTest constructor calls are Controller -> Dependency -> Helper
     * but in this case constructor calls are Helper -> Dependency -> Controller.
     */
    controller = new PleasantController(new PleasantDependency(new PleasantHelper()));
  }

  @Test
  public void thisTestCannotFailAsItIsIsolated() {
    controller.doSomething();
  }

  @Test
  public void thisTestCannotFailAsItIsIsolatedToo() {
    controller.doSomething();
  }
}
