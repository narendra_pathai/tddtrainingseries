package org.tddseries.localdate;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

import static org.mockito.Mockito.verify;

public class DiwaliPromotionFactory2Test {

  private static final LocalDateTime FIXED_DATE =
      LocalDateTime.of(2015, 3, 13, 14, 23);

  @Rule
  public MockitoRule mockito = MockitoJUnit.rule();

  @Spy
  private DiwaliDayProvider mockDiwaliDayProvider;

  @Test
  public void locatesCurrentYearDiwaliDay() {
    Clock fixedClock = newFixedClockAt(FIXED_DATE);
    DiwaliPromotionFactory2 factory = new DiwaliPromotionFactory2(mockDiwaliDayProvider, fixedClock);
    factory.get();
    // Because date is fixed, tests will always pass
    verify(mockDiwaliDayProvider).get(FIXED_DATE.getYear());
  }

  private Clock newFixedClockAt(LocalDateTime date) {
    return Clock.fixed(date.toInstant(ZoneOffset.UTC), ZoneId.systemDefault());
  }
}
