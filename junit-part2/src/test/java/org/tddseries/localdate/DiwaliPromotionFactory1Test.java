package org.tddseries.localdate;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Date;

import static org.mockito.Mockito.*;

public class DiwaliPromotionFactory1Test {
  @Rule
  public MockitoRule mockito = MockitoJUnit.rule();

  @Spy
  private DiwaliDayProvider mockDiwaliDayProvider;

  @Test
  public void locatesCurrentYearDiwaliDay() {
    DiwaliPromotionFactory1 factory = new DiwaliPromotionFactory1(mockDiwaliDayProvider);
    factory.get();
    // Well works all well in 2018, but starts failing in 2019
    verify(mockDiwaliDayProvider).get(2018);
  }
}
