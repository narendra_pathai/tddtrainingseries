package org.tddseries.mockito;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 * WARNING: Examples given in this class are just for learning Mocking library. In reality don't verify your own stubbings,
 * it makes no sense.
 * Anti practices:
 * 1) Verifying our own stubbings
 * 2) Violating principle "Don't mock third party classes (atleast not the ones that are hard to mock)"
 */
public class MockitoTest {

  @Rule public ExpectedException exception = ExpectedException.none();

  // Rule will automatically create init mocks which have annotation
  @Rule public MockitoRule mockito = MockitoJUnit.rule();
  @Mock private List<String> mockedList;
  @Mock private List<String> firstMock;
  @Mock private List<String> secondMock;
  @Mock private List<String> thirdMock;

  @Test
  @SuppressWarnings("unchecked")
  public void creatingAMockUsingMethod() {
    List<String> mockedList = Mockito.mock(List.class);

    assertThat(mockedList, is(notNullValue()));
  }

  @Test
  public void creatingAMockUsingRule() {
    // Mock will already be created for use
    assertThat(mockedList, is(notNullValue()));
  }

  @Test
  public void simpleVerification() {
    // perform some interactions, mock remembers all interactions
    mockedList.add("one");
    mockedList.clear();

    // Verification of interactions, verify selectively
    Mockito.verify(mockedList).add("one");
    Mockito.verify(mockedList).clear();
  }

  @Test
  public void simpleStubbingReturnValueFromMethod() {
    // Stubbing or training the mock
    Mockito.when(mockedList.get(0)).thenReturn("one");

    // Using the stubbed mock
    assertThat(mockedList.get(0), is(equalTo("one")));

    // Because we have not trained the mock to expect argument 1
    assertThat(mockedList.get(1), is(nullValue()));
  }

  @Test
  public void stubbingExceptionFromMethod() {
    // Train mocked list to throw exception when called with argument 1
    Mockito.when(mockedList.get(1)).thenThrow(new RuntimeException("Stubbing exception"));

    exception.expect(RuntimeException.class);
    exception.expectMessage("Stubbing exception");
    mockedList.get(1);
  }

  @Test
  public void verifyExactNumberOfInvocations() {
    mockedList.add("one");
    mockedList.add("two"); mockedList.add("two");
    mockedList.add("three"); mockedList.add("three"); mockedList.add("three");

    // verify exact invocations
    Mockito.verify(mockedList).add("one");
    Mockito.verify(mockedList, Mockito.times(1)).add("one");
    Mockito.verify(mockedList, Mockito.times(2)).add("two");
    Mockito.verify(mockedList, Mockito.times(3)).add("three");
  }

  @Test
  public void verifyAtleastOrAtmostInvocations() {
    mockedList.add("one");
    mockedList.add("two"); mockedList.add("two");
    mockedList.add("three"); mockedList.add("three"); mockedList.add("three");

    // verify atleast invocations
    Mockito.verify(mockedList, Mockito.atLeastOnce()).add("two");
    Mockito.verify(mockedList, Mockito.atLeast(2)).add("three");
    Mockito.verify(mockedList, Mockito.atMost(5)).add("three");
  }


  @Test
  public void verifyNeverInvocations() {
    mockedList.add("one");

    // verify atleast invocations
    Mockito.verify(mockedList, Mockito.never()).add("two");
    Mockito.verify(mockedList, Mockito.never()).add("three");
  }

  @Test
  public void stubbingExceptionFromVoidMethod() {
    Mockito.doThrow(RuntimeException.class).when(mockedList).clear();

    exception.expect(RuntimeException.class);

    mockedList.clear();
  }

  @Test
  public void verifyInorderInvocationWithSingleMock() {
    mockedList.add("added first");
    mockedList.add("added second");

    InOrder inOrder = Mockito.inOrder(mockedList);
    inOrder.verify(mockedList).add("added first");
    inOrder.verify(mockedList).add("added second");
  }

  @Test
  public void verifyInorderInvocationWithMultipleMocks() {
    firstMock.add("first");
    secondMock.add("second");
    thirdMock.add("third");

    // Not necessary to include all mocks
    InOrder inOrder = Mockito.inOrder(firstMock, thirdMock);
    inOrder.verify(firstMock).add("first");
    inOrder.verify(thirdMock).add("third");
  }

  @Test
  public void verifyNoInteractionWithMock() {
    firstMock.add("first");

    // Verify no method of other mocks was called
    Mockito.verifyZeroInteractions(secondMock, thirdMock, mockedList);
  }


  /*
   * Advanced mockito features
   */

  @Test
  public void stubbingReturnValueForAnyArgument() {
    // For any index value return "fixed value"
    // List will behave as if all indexes have value "fixed value"
    Mockito.when(mockedList.get(Mockito.anyInt())).thenReturn("fixed value");

    assertThat(mockedList.get(999), is(equalTo("fixed value")));
  }

  @Test
  public void verifyingArgumentUsingArgumentMatchers() {
    mockedList.get(987);
    // Will test if get() was called with any index argument
    Mockito.verify(mockedList).get(Mockito.anyInt());
  }

}
