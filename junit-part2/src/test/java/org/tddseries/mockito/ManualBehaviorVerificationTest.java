package org.tddseries.mockito;

import org.junit.Test;
import org.tddseries.mockito.WeatherObservable;
import org.tddseries.mockito.WeatherObserver;

import java.util.Observable;
import java.util.Observer;

import static org.junit.Assert.assertEquals;

public class ManualBehaviorVerificationTest {

  @Test
  public void cannotUserRealObserver() {
    WeatherObservable observable = new WeatherObservable();
    WeatherObserver observer = new WeatherObserver();
    observable.addObserver(observer);

    // exercise the SUT
    observable.setWeather("FALL");

    // Verify that observer was notified
    // How to verify that?
  }

  @Test
  public void notifiesObserversOfChangeInWeatherCondition() {
    WeatherObservable observable = new WeatherObservable();
    // Custom hand rolled observer
    TestObserver observer = new TestObserver();
    observable.addObserver(observer);

    // exercise the SUT
    observable.setWeather("FALL");

    // Manual behavior verification
    assertEquals(1, observer.count);
    assertEquals("FALL", observer.arg);
  }

  // A test specific observer that tracks if method was called
  class TestObserver implements Observer {
    private int count = 0;
    private Object arg;

    @Override
    public void update(Observable o, Object arg) {
      this.count++;
      this.arg = arg;
    }
  }
}
