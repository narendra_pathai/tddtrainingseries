package org.tddseries.mockito;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.tddseries.mockito.WeatherObservable;

import java.util.Observer;

import static org.mockito.Mockito.*;

public class MockitoBehaviorVerificationTest {
  @Rule
  public MockitoRule mockito = MockitoJUnit.rule();

  @Mock
  private Observer mockObserver1;

  @Mock
  private Observer mockObserver2;

  @Test
  public void notifiesObserversOfChangeInWeatherCondition() {
    WeatherObservable observable = new WeatherObservable();
    observable.addObserver(mockObserver1);
    observable.addObserver(mockObserver2);

    // Winter is coming :P
    observable.setWeather("WINTER");

    verify(mockObserver1).update(observable, "WINTER");
    verify(mockObserver2).update(observable, "WINTER");
  }

}
