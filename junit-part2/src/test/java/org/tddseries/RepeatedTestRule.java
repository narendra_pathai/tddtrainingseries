package org.tddseries;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class RepeatedTestRule implements TestRule {

  @Override
  public Statement apply(Statement base, Description description) {
    Statement result = base;
    Repeat repeat = description.getAnnotation(Repeat.class);
    if (repeat != null) {
      result = new RepeatedStatement(base, repeat.value());
    }
    return result;
  }

  @Target(ElementType.METHOD)
  @Retention(RetentionPolicy.RUNTIME)
  public @interface Repeat {
    int value();
  }

  private class RepeatedStatement extends Statement {
    private final Statement base;
    private final int times;

    public RepeatedStatement(Statement base, int times) {
      this.base = base;
      this.times = times;
    }

    @Override
    public void evaluate() throws Throwable {
      for (int i = 0; i < times; i++) {
        base.evaluate();
      }
    }
  }
}
