package org.tddseries;

import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

public class StateVerificationTest {

  @Test
  public void onAddingAnElementToEmptyListItIsNoMoreEmpty() {
    ArrayList<Integer> list = new ArrayList<>();

    // Exercise the system under test
    list.add(1);

    // State verification - verify list is no more empty
    assertFalse(list.isEmpty());
  }

  @Test
  public void aNewlyCreatedThreadIsInNewState() {
    // Create a new thread
    Thread t = new Thread(() -> {});

    // State verification - Verify it is is NEW state
    assertThat(t.getState(), is(equalTo(Thread.State.NEW)));
  }
}
