package org.tddseries.thermostat;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * Demonstrates the use of following:
 * 1) Stubbing mocks to return value in this case stubbing sensor to return fixed value when temperature
 * is sensed
 * 2) Verifying exact invocations. In this case checking that listener was invoked at least once.
 */
@RunWith(JUnitParamsRunner.class)
public class Thermostat3Test {

  @Rule
  public MockitoRule mockito = MockitoJUnit.rule();

  @Mock
  private TemperatureSensor mockSensor;

  @Mock
  private TemperatureListener mockListener;

  private Thermostat3 thermostat;

  @Before
  public void setUp() {
    thermostat = new Thermostat3(24, mockSensor);
    thermostat.addListener(mockListener);
  }

  @Test
  public void printsTooHotAlertIfTempratureExceedsTargetTemperatureThreshold() throws InterruptedException {
    when(mockSensor.getReading()).thenReturn(24.11);

    Thread.sleep(2000);

    // We are verifying atleast once because in 2 seconds there is a possibility that temperature
    // was sensed twice
    verify(mockListener, atLeastOnce()).onTemperatureTooHigh(24.11);
  }

  @Test
  public void printsTooColdAlertIfTempraturePlumetsBelowTargetTemperatureThreshold() throws InterruptedException {
    when(mockSensor.getReading()).thenReturn(23.89);

    Thread.sleep(2000);

    verify(mockListener, atLeastOnce()).onTemperatureTooLow(23.89);
  }

  @Test
  @Parameters(method = "dataFor_printsNoAlertIfTempratureIsWithinTargetTemperatureThreshold")
  public void printsNoAlertIfTempratureIsWithinTargetTemperatureThreshold(double temperature) throws InterruptedException {
    when(mockSensor.getReading()).thenReturn(temperature);

    Thread.sleep(2000);

    verifyZeroInteractions(mockListener);
  }

  private Object[] dataFor_printsNoAlertIfTempratureIsWithinTargetTemperatureThreshold() {
    return new Object[] {
        24,
        23.91,
        24.09
    };
  }

  @After
  public void cleanUp() throws InterruptedException {
    thermostat.stop();
  }
}
