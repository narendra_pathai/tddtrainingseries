package org.tddseries.thermostat;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class Chip121TemperatureSensorTest {

  private static final double TARGET_TEMPERATURE = 24;
  /**
   * Spy is used because we want to verify that thermostat passes correct temperature range to random provider
   * and at the same time we want to provide fake random. Using fake random makes it reusable to use across
   * multiple classes.
   */
  @Spy
  private FixedRandomProvider fixedRandomProvider = new FixedRandomProvider();
  private TemperatureSensor sensor;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
    sensor = new Chip121TemperatureSensor(TARGET_TEMPERATURE, fixedRandomProvider);
  }

  @Test
  public void sensorGeneratesTemperatureWithinPlusOrMinusOneRangeOfTargetTemperature() {
    sensor.getReading();
    // Tests are incomplete without this assertion. Why?
    verify(fixedRandomProvider, times(1)).nextDouble(TARGET_TEMPERATURE - 1.0, TARGET_TEMPERATURE + 1.0);
  }

  @Test
  public void returnsTheCurrentTemperatureReading() {
    fixedRandomProvider.setRandom(24);

    assertThat(sensor.getReading(), is(equalTo(24.0)));
  }

  private class FixedRandomProvider implements RandomProvider {

    private double random;

    @Override
    public double nextDouble(double from, double to) {
      return random;
    }

    public void setRandom(double random) {
      this.random = random;
    }
  }
}
