package org.tddseries.thermostat;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Demonstrates the use of following:
 * 1) Creating mocks using @Mock annotation and MockitoRule
 * 2) Dependency injecting mockSensor in Thermostat
 * 3) A simple data driven test case using data method
 */
@RunWith(JUnitParamsRunner.class)
public class Thermostat2Test {

  @Rule public MockitoRule mockito = MockitoJUnit.rule();

  @Mock
  private TemperatureSensor mockSensor;

  private ByteArrayOutputStream out;
  private PrintStream old;

  private Thermostat2 thermostat;

  @Before
  public void setUp() {
    old = System.out;
    out = new ByteArrayOutputStream();
    System.setOut(new PrintStream(out));
    thermostat = new Thermostat2(24, mockSensor);
  }

  @Test
  public void printsTooHotAlertIfTempratureExceedsTargetTemperatureThreshold() throws InterruptedException {
    when(mockSensor.getReading()).thenReturn(24.11);

    Thread.sleep(2000);
    String message = out.toString();

    assertThat(message, CoreMatchers.containsString(String.format("Temperature %f is too high", 24.11)));
  }

  @Test
  public void printsTooColdAlertIfTempraturePlumetsBelowTargetTemperatureThreshold() throws InterruptedException {
    when(mockSensor.getReading()).thenReturn(23.89);

    Thread.sleep(2000);
    String message = out.toString();

    assertThat(message, CoreMatchers.containsString(String.format("Temperature %f is too low", 23.89)));
  }

  @Test
  @Parameters(method = "dataFor_printsNoAlertIfTempratureIsWithinTargetTemperatureThreshold")
  public void printsNoAlertIfTempratureIsWithinTargetTemperatureThreshold(double temperature) throws InterruptedException {
    when(mockSensor.getReading()).thenReturn(temperature);

    Thread.sleep(2000);
    String message = out.toString();

    assertTrue(message.isEmpty());
  }

  private Object[] dataFor_printsNoAlertIfTempratureIsWithinTargetTemperatureThreshold() {
    return new Object[] {
        24,
        23.91,
        24.09
    };
  }

  @After
  public void cleanUp() throws InterruptedException {
    System.setOut(old);
    thermostat.stop();
  }
}
