package org.tddseries.thermostat;

import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.tddseries.RepeatedTestRule;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * This is pretty much worst case when writing unit test case for legacy code.
 * 1) We don't have control over which case we are testing because the temperature is being sensed randomly.
 * 2) It is not possible to hit all scenarios again because source of randomness is not in control - That's why have to repeat test
 * several times with the hope of hitting all the branches
 * 3) Because the sensor is taking 1 sec to sense, we have to wait for 2 secs before thermostat changes
 * its state. Which is yuck!! Unit test cases should avoid use of threads because,
 *  a) as it makes tests difficult to understand and read.
 *  b) tests become slower because Thread.sleep() is sprinkled all over test suite
 *  c) Using threads can make tests flaky because it becomes hard to guarantee order in which execution is done.
 * 4) Thermostat is directly logging in System.out, so we need to fake that as well. But System.out is a global state
 * because of static. So running all tests in parallel may cause issues.
 * 5) Production business logic is repeated in test code as well, because there is no other way to test the code.
 * This violates the DRY (Don't Repeat Yourself) principle.
 */
public class Thermostat1Test {

//  @Rule public RepeatedTestRule repeat = new RepeatedTestRule();

  private ByteArrayOutputStream out;
  private PrintStream old;

  @Before
  public void fakeSysOut() {
    old = System.out;
    out = new ByteArrayOutputStream();
    System.setOut(new PrintStream(out));
  }

  @Test
  @RepeatedTestRule.Repeat(10)
  public void test() throws InterruptedException {
    Thermostat1 thermostat1 = new Thermostat1(24);
    Thread.sleep(2000);
    String message = out.toString();
    double temperature = thermostat1.getCurrentTemperature();
    // Now because the temperature is not in our control We cannot predict whether the output will be temprature
    // too low or too high or just fine. Hence we have to check that ourselves
    if (tooHot(temperature, 24)) {
      assertThat(message, CoreMatchers.containsString(String.format("Temperature %f is too high", temperature)));
    } else if (tooCold(temperature, 24)) {
      assertThat(message, CoreMatchers.containsString(String.format("Temperature %f is too low", temperature)));
    } else {
      assertTrue(message.isEmpty());
    }
  }

  private boolean tooCold(double temperature, double targetTemperature) {
    return Double.compare(temperature, targetTemperature) < -0.1;
  }

  private boolean tooHot(double temperature, double targetTemperature) {
    return Double.compare(temperature, targetTemperature) > 0.1;
  }

  @After
  public void resetSysOut() {
    System.setOut(old);
  }
}