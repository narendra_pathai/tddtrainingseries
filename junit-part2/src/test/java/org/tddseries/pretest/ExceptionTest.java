package org.tddseries.pretest;

import org.junit.Test;

import java.util.EmptyStackException;
import java.util.Stack;

public class ExceptionTest {

  private Stack<Integer> stack = new Stack<>();

  @Test
  public void poppingAnEmptyStackThrowsException() {
    stack.add(1);
    stack.pop();
    try {
      stack.pop();
    } catch (EmptyStackException ex) {
      // pass
    }
  }
}
