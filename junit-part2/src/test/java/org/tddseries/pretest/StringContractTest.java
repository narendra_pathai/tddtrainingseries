package org.tddseries.pretest;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class StringContractTest {

  @Test
  public void _______() {
    String s1 = "FB";
    String s2 = "Ea";
    assertEquals(s1.hashCode(), s2.hashCode());
    assertNotEquals(s1, s2);
  }

  @Test
  public void ________() {
    String s1 = "FB";
    String s2 = "FB";
    assertEquals(s1.hashCode(), s2.hashCode());
  }
}
