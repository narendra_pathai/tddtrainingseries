package org.tddseries.pretest;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ListTest {

  private static List<Integer> list = new ArrayList<>();

  @Test
  public void aNewlyCreatedListIsEmpty() {
    assertTrue(list.isEmpty());
  }

  @Test
  public void onAddingAnElementRemainsNoLongerEmpty() {
    list.add(1);

    assertFalse(list.isEmpty());
  }
}
