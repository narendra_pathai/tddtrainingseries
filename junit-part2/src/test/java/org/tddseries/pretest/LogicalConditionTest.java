package org.tddseries.pretest;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class LogicalConditionTest {
  int x = 5;
  boolean y = false;
  int z = 10;

  @Test
  public void test() {
    if (x > 5) {
      assertTrue(y);
    } else if (x == 5) {
      assertEquals(x, z);
    } else {
      assertEquals(10, z);
    }
  }
}
