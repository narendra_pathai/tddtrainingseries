package org.tddseries.pretest;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Stack;

import static org.junit.Assert.*;

@RunWith(JUnitParamsRunner.class)
public class DataDrivenTest {

  private Stack<Integer> stack = new Stack<>();

  @Test
  @Parameters("dataFor_testPop")
  public void testPop(int pushCount, int popCount, Exception expectedException) {
    pushRandomElements(pushCount);
    try {
      for (int i = 0; i < popCount; i++) {
        stack.pop();
      }
      if (expectedException != null) {
        fail();
      }
    } catch (Exception ex) {
      assertEquals(expectedException, ex);
    }
  }

  private void pushRandomElements(int pushCount) {
  }

  public Object[] dataFor_testPop() {
    return null;
  }
}
