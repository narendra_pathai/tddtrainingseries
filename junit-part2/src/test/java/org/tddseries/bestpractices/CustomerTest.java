package org.tddseries.bestpractices;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class CustomerTest {

  /**
   * This is example of how unit tests should not be written. Because it is testing internals of Customer
   * class
   */
  @Test
  public void promotingChangesCustomerStatusToPreferred() {
    Customer customer = new Customer();
    customer.promote();
    // Exposing internal state makes tests brittle, what if tomorrow
    // we have to refactor and remove enum to use boolean
    assertThat(customer.status, is(equalTo(Customer.CustomerStatus.PREFERRED)));
  }

  /**
   * This is the preferred way to unit test. Because it is testing only the publicly visible side effect of calling
   * promote method. The visible side effect is that customer gets 50% discount
   */
  @Test
  public void aPromotedCustomerEnjoysDiscountedRate() {
    Customer customer = new Customer();
    customer.promote();
    // Checking for side effect using public API and not checking
    // internal state. So Customer can be refactored to use boolean
    // instead of enum easily without breaking unit tests
    assertThat(customer.getDisount(), is(equalTo(0.5)));
  }
}