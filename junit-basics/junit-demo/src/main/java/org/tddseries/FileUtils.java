package org.tddseries;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;

public class FileUtils {

	public static String readAsString(File downloadedFile) throws IOException {
		FileReader reader = null;
		try {
			reader = new FileReader(downloadedFile);
			StringWriter writer = new StringWriter();

			char[] buffer = new char[255];
			int n = 0;
			while (-1 != (n = reader.read(buffer))) {
				writer.write(buffer, 0, n);
			}
			return writer.toString();
		} finally {
			Closeables.closeQuietly(reader);
		}
	}

}
