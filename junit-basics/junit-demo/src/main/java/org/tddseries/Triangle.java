package org.tddseries;


public class Triangle {

    private int side1;
    private int side2;
    private int side3;


    public Triangle() {
        this(0, 0, 0);
    }

    public Triangle(int side1, int side2, int side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public String determineTriangleType() throws Exception {

        // Check that nothing is negative
        if (side1 <= 0 || side2 <= 0 || side3 <= 0) {
            throw new Exception("At least one length is less than 0!");
        }

        // Check for side length
        if ((side1 + side2 <= side3) || (side1 + side3 <= side2) || (side2 + side3 <= side1)) {
            throw new Exception("The lengths of the triangles do not form a valid triangle!");
        }

        String type = "";

        if ((side1 == side2) && (side2 == side3)) {
            type= "Equilateral";
        } else if (( side1 == side2) || (side2 == side3) || (side1 == side3)) {
            type = "Isosceles";
        } else {
            type = "Scalene";
        }

        return type;
    }


}
