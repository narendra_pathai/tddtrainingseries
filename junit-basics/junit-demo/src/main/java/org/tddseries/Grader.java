package org.tddseries;

import static org.tddseries.Objects.checkArgument;

import javax.annotation.Nonnull;

public class Grader {

    private final int total;

    public Grader(int total) {
        this.total = total;
    }

    public @Nonnull Grade grade(int marks) {

        checkArgument(inRange(marks), "marks(" + marks + ") should be between 0 to " + total);

        int percentage = calculatePercentage(marks);

        return assignGrade(percentage);
    }

    private @Nonnull Grade assignGrade(int percentage) {
        Grade gradeReceive = Grade.F;
        for (Grade grade : Grade.values()) {

            if (grade.inRange(percentage)) {
                gradeReceive = grade;
            }
        }

        return gradeReceive;
    }

    private int calculatePercentage(int marks) {
        return marks * 100 / total;
    }

    private boolean inRange(int marks) {
        return NumberUtils.inRange(0, total, marks);
    }
}
