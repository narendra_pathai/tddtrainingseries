package org.tddseries;

import javax.annotation.Nonnull;


public class Objects {

    public static <T> T checkNotNull(@Nonnull T argument, @Nonnull String message) {
            if (argument == null) {
                throw new NullPointerException(message);
            }

            return argument;
    }

    public static void checkArgument(boolean expression, String message) {
        if (expression == false) {
            throw new IllegalArgumentException(message);
        }
    }
}
