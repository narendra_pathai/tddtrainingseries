package org.tddseries;

public class Thermostat {
    private int currentTemp;
    private int desiredTemp;

    public Thermostat(int currentTemp, int desireTemp) {
        this.currentTemp = currentTemp;
        desiredTemp = desireTemp;
    }
    public int  getCurrentTemp() { return currentTemp; }

    public int  getDesiredTemp() { return desiredTemp; }

    private void setDesiredTemp(int t) {
        desiredTemp = t;
    }

    public void increaseDesiredTemp() {
        setDesiredTemp(desiredTemp + 5);
    }
    public void decreaseDesiredTemp() {
        setDesiredTemp(desiredTemp - 5);
    }
}