package org.tddseries;


public interface Predicate<T> {
    public boolean apply(T t);
}
