package org.tddseries;


public class BankAccount {

    public long balance;
    public long minimumBalance;

    public BankAccount(long initialBalance, long minimumBalance) {
        this.balance = initialBalance;
        this.minimumBalance = minimumBalance;
    }


    public void deposit(long amount) {
        balance += amount;
    }

    public void withdraw(long withdrawAmount) throws Exception {
        long remainingBalance = balance - withdrawAmount;
        if (minimumBalance > remainingBalance) {
            throw new Exception("Current balance after withdrawal will less then Minimum balance");
        }

        balance = remainingBalance;

    }

    public long getBalance() {
        return balance;
    }
}
