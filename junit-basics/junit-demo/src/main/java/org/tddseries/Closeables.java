package org.tddseries;

import java.io.Closeable;

public class Closeables {

	public static void closeQuietly(Closeable closeable) {
		try {
			if (closeable != null) {
				closeable.close();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
