package org.tddseries;

public class Predicates {

    private static class NotNullPredicate implements Predicate<Object> {

		@Override
		public boolean apply(Object ref) {
			return ref != null;
		}
    }
    
    private static final Predicate<Object> NOT_NULL = new NotNullPredicate();
    
    @SuppressWarnings("unchecked")
	public static <T> Predicate<T> notNull() {
    	return (Predicate<T>) NOT_NULL;
    }
}
