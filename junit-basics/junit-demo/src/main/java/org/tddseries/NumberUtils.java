package org.tddseries;

public class NumberUtils {

    public static boolean isPositiveInteger(long number) {
        return number >=0;
    }

    public static boolean inRange(long from, long to, long number) {
        return number >= from && number <= to;
    }


}

