package org.tddseries;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

public class FileDownloader {

	private final File baseFolder;

	public FileDownloader(File baseFolder) {
		Objects.checkArgument(!baseFolder.isFile(), "base directory must be a directory");
		this.baseFolder = baseFolder;
	}

	public File downloadFromUrl(String urlString) throws IOException {
		URL url = new URL(urlString);
		String fileName = urlString.substring( urlString.lastIndexOf('/') + 1, urlString.length());

		BufferedInputStream inputStream = null;
		FileOutputStream writer = null;
		try {
			inputStream = new BufferedInputStream(url.openStream());
			File downloadedFile = new File(baseFolder.getAbsolutePath() + File.separator + fileName);
			writer = new FileOutputStream(downloadedFile);
			byte[] buffer = new byte[255];
			int n = 0;

			while (-1 != (n = inputStream.read(buffer))) {
				writer.write(buffer, 0, n);
			}

			return downloadedFile;
		} finally {
			Closeables.closeQuietly(inputStream);
			Closeables.closeQuietly(writer);
		}
	}
}
