package org.tddseries;

import javax.annotation.Nullable;


public class StringUtils {

    public static boolean isNullOrBlank(@Nullable String val) {
        return val == null || val.trim().isEmpty();
    }
}
