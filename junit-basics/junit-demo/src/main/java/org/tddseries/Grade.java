package org.tddseries;

public enum  Grade {
    A_PLUS("A+", 91, 100),
    A("A", 81, 90),
    A_MINUS("A-", 71, 80),
    B_PLUS("B+", 61, 70),
    B("B", 51, 60),
    B_MINUS("B-", 35, 50),
    F("F", 0, 34),
    ;

    private final String grade;
    private final int from;
    private final int upto;

    Grade(String grade, int from, int upto) {

        this.grade = grade;
        this.from = from;
        this.upto = upto;
    }

    public String toString() {
        return grade;
    }

    public boolean inRange(long percentage) {
        return from <= percentage && upto >= percentage;
    }
}
