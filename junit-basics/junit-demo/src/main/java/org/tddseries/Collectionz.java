package org.tddseries;

import java.util.Collection;
import java.util.Iterator;

public class Collectionz {

    public static <T> void filter(Collection<T> collection, Predicate<T> predicate) {
        Iterator<T> iterator = collection.iterator();
        while (iterator.hasNext()) {
            if (predicate.apply(iterator.next())) {
                iterator.remove();
            }

        }
    }
}
