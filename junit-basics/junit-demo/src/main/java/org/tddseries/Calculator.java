package org.tddseries;

import javax.annotation.Nonnull;

public class Calculator {

    public int add(@Nonnull int... summands) {


        int sum = 0;

        for (int summand : summands) {
            sum += summand;
        }

        return sum;
    }

    public int subtract(int minuend, @Nonnull int... subtrahends) {

        int difference = minuend;

        for (int subtrahend : subtrahends) {
            difference -= subtrahend;
        }

        return difference;
    }
}
