package org.tddseries;

import java.util.List;
import java.util.stream.Stream;

import javax.annotation.Nonnull;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * A page object representing Google Search page.
 * 
 */
public class GoogleSearchPage {

	private final WebDriver driver;

	public GoogleSearchPage(@Nonnull WebDriver driver) {
		this.driver = driver;
	}

	/**
	 * Opens the google basic search page.
	 */
	public void open() {
		driver.get("http://www.google.co.in/");
	}

	/**
	 * Performs basic search for the given search query on google search page.
	 */
	public void searchFor(String searchQuery) throws InterruptedException {
		driver.findElement(By.name("q")).sendKeys(searchQuery);
		driver.findElement(By.name("btnG")).click();
		
		waitForSearchFieldsToLoad();
	}

	private void waitForSearchFieldsToLoad() throws InterruptedException {
		Thread.sleep(1000);
	}

	/**
	 * @return the list of titles of search results on first page.
	 */
	public Stream<String> searchResults() {
		List<WebElement> weblinks = driver.findElements(By.cssSelector(".srg >.g >.rc >.r"));
		
		return weblinks.stream().map((webElement) -> webElement.getText()); 
	}

}
