package org.tddseries;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class FileDownloaderTest {
	
	@Rule public TemporaryFolder folder = new TemporaryFolder();
	
	@Test
	public void givenURLIsValid_DownloadsTheFileFromThatURL() throws IOException {
		// arrange
		File temp = folder.newFolder("filedownloader");
		FileDownloader downloader = new FileDownloader(temp);
		
		// act
		File downloadedFile = downloader.downloadFromUrl(new File("hello.txt").toURI().toURL().toString());
		
		// assert
		assertFileContentEquals(downloadedFile, "hello");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void constructorThrowsIllegalArgumentExceptionIfProvidedFileIsNotADirectory() throws IOException {
		// arrange
		File file = folder.newFile("dummy.txt");
		
		// act
		new FileDownloader(file);
	}

	private static void assertFileContentEquals(File downloadedFile, String expectedContents) throws IOException {
		String contents = FileUtils.readAsString(downloadedFile);
		Assert.assertEquals(expectedContents, contents);
	}
}
