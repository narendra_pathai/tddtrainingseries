package org.tddseries;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Created by harsh on 9/24/16.
 */
public class ThreadTest {

    private Thread thread;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void createThread() {
        thread = new Thread();
    }

    @Test
    public void startingTheAlreadyStartedThreadThrowIllegalThreadStateException() {
        startThread();
        exception.expect(IllegalThreadStateException.class);
        thread.start();
    }


    private void startThread() {
        thread.start();;
    }
}
