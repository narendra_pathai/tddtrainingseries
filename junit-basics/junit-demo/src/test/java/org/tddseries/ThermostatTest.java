package org.tddseries;


import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

public class ThermostatTest {

    private Thermostat thermostat;

    @Before
    public void createThermostat() {
        thermostat = new Thermostat(45, 25);
    }

    @Test
    public void incrementDesireTemperatureIncreasesTheDesireTemperatureByFiveUnits() {

        int desiredTemp = thermostat.getDesiredTemp();

        thermostat.increaseDesiredTemp();

        assertThat(thermostat.getDesiredTemp(), equalTo(desiredTemp + 5));
    }

    @Test
    public void decrementDesireTemperatureDecreasesTheDesireTemperatureByFiveUnits() {

        int desiredTemp = thermostat.getDesiredTemp();

        thermostat.decreaseDesiredTemp();

        assertThat(thermostat.getDesiredTemp(), equalTo(desiredTemp - 5));
    }

}
