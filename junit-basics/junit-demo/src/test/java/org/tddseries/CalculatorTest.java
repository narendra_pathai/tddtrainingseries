package org.tddseries;

import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {

	@Test
	public void test() {
		Calculator calculator = new Calculator();
		int sum = calculator.add(1, 2);
		if (sum == 3) {
			System.out.println("Test case passes");
		} else {
			throw new AssertionError("Sum is not 3");
		}
	}
	
	@Test
	public void test1() {
		Calculator calculator = new Calculator();
		int sum = calculator.add(1, 2);
		
		assert sum == 3; //But this needs -ea flag to be enabled while running the test cases. So this is error prone.
	}
	
	@Test
	public void test2() {
		Calculator calculator = new Calculator();
		int sum = calculator.add(1, 2);
		
		Assert.assertEquals(3, sum); // Junit provides rich set of assertion variations for making assertion easier.
	}
}
