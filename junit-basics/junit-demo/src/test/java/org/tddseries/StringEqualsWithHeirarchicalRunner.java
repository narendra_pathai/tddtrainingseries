package org.tddseries;

import de.bechte.junit.runners.context.HierarchicalContextRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(HierarchicalContextRunner.class)
public class StringEqualsWithHeirarchicalRunner {

    private String dept = "CSM";

    public class SameString {

        public String sameDept = "CSM";

        @Test
        public void isReflexive() {
            assertTrue(dept.equals(dept));
        }

        @Test
        public void isSymetric() {
            /*
            Dept == sameDept
            sameDept == Dept
             */
            assertThat(dept.equals(sameDept), sameInstance(sameDept.equals(dept)));
        }

        @Test
        public void isTransitive() {
            /*
            Dept == sameDept
            sameDept == "CSM"

            then Dept == "CSM"
             */
            assertThat(dept.equals("CSM"), allOf(equalTo(dept.equals(sameDept)), equalTo(sameDept.equals("CSM"))));
        }
    }

    public class DifferentString {

        public String anotherDept = "CSMs";
        @Test
        public void isReflexive() {
            assertTrue(dept.equals(dept));
        }

        @Test
        public void isSymetric() {
            assertThat(dept.equals(anotherDept), sameInstance(anotherDept.equals(dept)));
        }

        @Test
        public void isTransitive() {
            /*
            Dept != anotherDept
            anotherDept != "CSMs"

            then Dept != "CSMs"
             */
            assertThat(dept.equals("CSMs"), allOf(equalTo(dept.equals(anotherDept)), equalTo(anotherDept.equals("CSMss"))));
        }
    }

    @Test
    public void equalsWithNullReturnFalse() {
        assertFalse(dept.equals(null));
    }

}
