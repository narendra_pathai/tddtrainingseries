package org.tddseries;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

// We need to tell Junit to run it using Junit Params runner
@RunWith(JUnitParamsRunner.class)
public class StringTest {

	@Test
	public void toUpperCaseConvertsCharactersToUpperCase() {
		String input = "a";
		String output = input.toUpperCase();
		assertThat(output, equalTo("A"));
	}
	
	@Test
	// This is the most frequently seen anti pattern way of doing parameterized tests
	// There is a lot of duplication in code, which will become more and more difficult to maintain if the API changes
	// There is a lot of noise due to duplication
	// Limitation is that if first test input fails no other input will be tested for
	// There is violation of AAA pattern
	public void oneWayToDoParameterizedTest() {
		String input = "a";
		String output = input.toUpperCase();
		assertThat(output, equalTo("A"));
		
		input = "b";
		output = input.toUpperCase();
		assertThat(output, equalTo("B"));
	}
	
	@Test
	// This is much better, has less repetition, but still has the problem that if first data fails
	// then the others will not be executed.
	// Also the individual tests run will not be visible in JUnit view. As Junit is not aware that
	// there are multiple cases in single test.
	// Wouldn't it be better if JUnit could provide us with arguments in the method itself
	// As well as the tests were visible in Junit view?
	public void otherWayToDoParameterizedTest() {
		List<String[]> testData = new ArrayList<>();
		testData.add(new String[] {"a", "A"});
		testData.add(new String[] {"b", "B"});
		testData.add(new String[] {"c", "C"});
		testData.add(new String[] {"C", "C"});
		testData.add(new String[] {"B", "B"});
		testData.add(new String[] {"A", "A"});
		
		for (String[] inputOutputPair : testData) {
			assertThat(inputOutputPair[0].toUpperCase(), equalTo(inputOutputPair[1]));
		}
	}
	
	// Writing Data driven test case using JunitParams library
	@Test
	// We will need to add dependency for JunitParams in our project.
	// Junit params dependency is now added
	@Parameters({"a, A", "b, B", "A, A", "B, B"})
	public void passingMethodParamsUsingAnnotation(String input, String expectedOutput) {
		assertThat(input.toUpperCase(), equalTo(expectedOutput));
	}
	
	@Test
	// Now we will pass the method arguments using data method
	@Parameters(method = "dataFor_passingParamsUsingMethod")
	public void passingParamsUsingMethod(String input, String expectedOut) {
		assertThat(input.toUpperCase(), equalTo(expectedOut));
	}
	
	// Now we need to create a method (static) for providing the data
	public static Object[][] dataFor_passingParamsUsingMethod() {
		return new Object[][] {
			// This becomes data for first case. Each row represents single test data
			new Object[] {"a", "A"},
			
			new Object[] {"b", "B"},
			
			new Object[] {"A", "A"},
			
			new Object[] {"B", "B"}
		};
	}
	
	// Now we will test to upper case using Data class to pass the parameters
	@Test
	// Providing the data class from which the parameters will be extracted
	@Parameters(source = DataForToUpperCase.class)
	public void passingParamsUsingClass(String input, String expectedOutput) {
		assertThat(input.toUpperCase(), equalTo(expectedOutput));
	}
	
	// We will create a nested static class that contains test data for toUpperCase
	public static class DataForToUpperCase {
		
		// All the public static methods will be considered as test data inputs
		public static Object[][] provideLowerCaseValues() {
			return new Object[][] {
				new Object[] {"a", "A"},
				new Object[] {"b", "B"}
			};
		}
		
		public static Object[][] provideUpperCaseValues() {
			return new Object[][] {
				new Object[] {"A", "A"},
				new Object[] {"B", "B"}
			};
		}
	}
	
	@Test
	@FileParameters(value = "DataForToUpperCase.csv")
	public void passingDataUsingFile(String input, String expectedOutput) {
		assertThat(input.toUpperCase(), equalTo(expectedOutput));
	}
}