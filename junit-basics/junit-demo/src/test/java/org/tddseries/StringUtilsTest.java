package org.tddseries;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class StringUtilsTest {

	@Test
	public void testIsNullOrBlank_WhenPassedStringReferenceIsNull_ReturnsTrue() {
		assertTrue(StringUtils.isNullOrBlank(null));
	}
	
	@Test
	@Parameters(source = DataForIsNullOrBlank.class)
	public void testIsNullOrBlank_checksForBlankness(String inputString, boolean expectedOutput) {
		assertEquals(expectedOutput, StringUtils.isNullOrBlank(inputString));
	}
	
	public static class DataForIsNullOrBlank {
		
		public static Object[][] provideBlankStrings() {
			return new Object[][] {
				new Object[] {" ", true},
				new Object[] {"  ", true},
				new Object[] {"\t", true},
				//We can go paranoid and test all the cases, but as we already said we don't need to check
				//every possible case if we get the necessary confidence.
			};
		}
		
		public static Object[][] provideNonBlankStrings() {
			return new Object[][] {
				new Object[] {"a", false},
				new Object[] {" a", false},
				new Object[] {"a ", false},
				//We can go paranoid and test all the cases, but as we already said we don't need to check
				//every possible case if we get the necessary confidence.
			};
		}
	}
}
