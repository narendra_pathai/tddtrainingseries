package org.tddseries;

import static org.junit.Assert.assertThat;
import org.junit.Test;
import static org.tddseries.PersonMatchers.hasAge;
import static org.tddseries.PersonMatchers.isAdult;

public class PersonMatchersTest {

	@Test
	public void matchAgeUsingHamcrestMatchers() {
		Person person = new Person("Andy", 31);
		
		assertThat(person, hasAge(31));
	}

	@Test
	public void matchAdultnessUsingHamcrestMatchers() {

		Person person = new Person("Andy", 30);

		assertThat(person, isAdult());
	}
}
