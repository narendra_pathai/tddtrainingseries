package org.tddseries;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.EmptyStackException;
import java.util.Stack;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.bechte.junit.runners.context.HierarchicalContextRunner;

// First we need to tell Junit to use Hierarchical context runner library.
// So first adding the dependency of this library in our project.
@RunWith(HierarchicalContextRunner.class)
public class TestingAStackWithHierarchicalContext {

	//Now we can use nested contexts
	
	@Test
	public void isInstantiatedWithConstructor() {
		new Stack<>();
	}
	
	// This is a new context that tells us the situation that stack is newly created
	public class WhenNew {
		
		private Stack<Object> stack;

		@Before
		public void createANewStack() {
			stack = new Stack<>();
		}
		
		@Test
		// A newly created stack is empty
		public void isEmpty() {
			assertTrue(stack.isEmpty());
		}
		
		@Test(expected = EmptyStackException.class)
		public void throwsEmptyStackExceptionWhenPopped() {
			stack.pop();
		}
		
		@Test(expected = EmptyStackException.class)
		public void throwsEmptyStackExceptionWhenPeeked() {
			stack.peek();
		}

		// Writing a new context of the situation when an element is pushed into the stack
		public class AfterPushing {
			
			String anElement = "an element";
			
			@Before
			public void pushAnElement() {
				stack.push(anElement);
			}
			
			@Test
			public void isNoLongerEmpty() {
				assertFalse(stack.isEmpty());
			}
			
			@Test
			public void returnsElementWhenPopped() {
				assertSame(anElement, stack.pop());
			}
			
			@Test
			public void returnsElementWhenPeeked() {
				assertSame(anElement, stack.peek());
			}
		}
	}
}
