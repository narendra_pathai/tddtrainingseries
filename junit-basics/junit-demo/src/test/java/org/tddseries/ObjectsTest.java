package org.tddseries;

import static org.hamcrest.CoreMatchers.sameInstance;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ObjectsTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void throwsNullPointerExceptionWhenProvideNullInstance() {

        String exceptionMessage = "Argument is null";

        exception.expect(NullPointerException.class);
        exception.expectMessage(sameInstance(exceptionMessage));

        Person person = null;
        Objects.checkNotNull(person, exceptionMessage);
    }

    @Test
    public void throwsIllegalArgumentExceptionWhenExpressionEvaluatesToFalse() {

        String exceptionMessage = "Argument is not valid";

        exception.expect(IllegalArgumentException.class);
        exception.expectMessage(sameInstance(exceptionMessage));

        Objects.checkArgument(false, exceptionMessage);
    }
}
