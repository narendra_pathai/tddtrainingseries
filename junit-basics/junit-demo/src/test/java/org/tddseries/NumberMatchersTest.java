package org.tddseries;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static org.tddseries.NumberMatchers.isDivisibleBy;

public class NumberMatchersTest {

	@Test
	public void checkDivisibilityUsingHamcrestMatcher() {
		assertThat(6, isDivisibleBy(2));
	}
	
	@Test
	public void checkIndivisibilityUsingHamcrestMatcher() {
		assertThat(17, not(isDivisibleBy(2)));
	}
}
