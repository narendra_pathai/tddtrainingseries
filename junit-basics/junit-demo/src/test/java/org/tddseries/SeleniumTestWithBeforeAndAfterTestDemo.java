package org.tddseries;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumTestWithBeforeAndAfterTestDemo {

	private WebDriver driver;
	private GoogleSearchPage googleHomePage;
	
	@Before
	public void openWebDriver() {
		System.setProperty("webdriver.chrome.driver", "/media/narendra/WORK/LIBRARIES_AND_UTILS/chromedriver");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		googleHomePage = new GoogleSearchPage(driver);
	}
	
	@Test
	public void agileDataLinkIsTheFirstSearchResult() throws InterruptedException {
		googleHomePage.open();
		
		googleHomePage.searchFor("Test Driven Development");
		
		String firstResult = googleHomePage.searchResults().findFirst().get();
		
		Assert.assertEquals("Introduction to Test Driven Development (TDD) - Agile Data", firstResult);
	}
	
	@Test
	public void wikipediaLinkIsTheSecondSearchResult() throws InterruptedException {
		googleHomePage.open();
		
		googleHomePage.searchFor("Test Driven Development");
		
		String firstResult = googleHomePage.searchResults().skip(1).findFirst().get();
		
		Assert.assertEquals("Test-driven development - Wikipedia, the free encyclopedia", firstResult);
	}
	
	@After
	public void closeWebDriver() {
		driver.close();
	}
}
