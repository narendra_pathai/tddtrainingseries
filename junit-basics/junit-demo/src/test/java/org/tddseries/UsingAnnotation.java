package org.tddseries;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;


@RunWith(JUnitParamsRunner.class)
public class UsingAnnotation {


    @Test
    @Parameters(value = {
            "100,A_PLUS",
            "91, A_PLUS",
            "90, A",
            "81, A",
            "80, A_MINUS",
            "71, A_MINUS",
            "70, B_PLUS",
            "61, B_PLUS",
            "60, B",
            "51, B",
            "50, B_MINUS",
            "35, B_MINUS",
            "34, F",
            "0,  F"
    })
    public void assignedTestAssignedGradeAccordingToPercentageReceived(int marks, Grade expectedGrade) {

        Grader grader = new Grader(100);

        Grade assignedGrade = grader.grade(marks);

        assertThat(assignedGrade, equalTo(expectedGrade));


    }

}
