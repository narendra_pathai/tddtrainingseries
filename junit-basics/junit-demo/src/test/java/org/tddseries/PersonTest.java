package org.tddseries;


import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class PersonTest {

    @Test
    @Parameters(value = {"18, true", "17, false"})
    public void personAboveACertainAgeIsConsideredAdult(int age, boolean expectedResult) {
        Person person = new Person("Andy", age);
        assertEquals(expectedResult, person.isAdult());
    }
}
