package org.tddseries;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({StringEqualsWithHeirarchicalRunner.class,
        StringHashCode.class,
        StringTest.class})
public class StringTestSuite {

}
