package org.tddseries;


import de.bechte.junit.runners.context.HierarchicalContextRunner;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(HierarchicalContextRunner.class)
public class EmptyListTest {

    private List<Object> anEmptyList;



    @Before
    public void createList() {
        anEmptyList = Collections.emptyList();
    }


    public void isAlwaysEmpty() {
        assertTrue(anEmptyList.isEmpty());
    }



    public class isImmutable {
        @Rule
        public ExpectedException exception = ExpectedException.none();

        @Test
        public void addingAnElementThrowsUnsupportedOperationException() {
            exception.expect(UnsupportedOperationException.class);
            anEmptyList.add("test");
        }

        @Test
        public void removingAnElementFromIndexThrowsUnsupportedOperationException() {
            exception.expect(UnsupportedOperationException.class);
            anEmptyList.remove(0);
        }

        @Test
        public void removingAnElementReturnsFalse() {
            assertFalse(anEmptyList.remove("test"));
        }
    }




}
