package org.tddseries;


import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TriangleTest {


    @Rule public ExpectedException exception = ExpectedException.none();

    @Test
    public void allSidesAreOfEqualLengthThenEquilateral() throws Exception {

        int triangleSide = 100;

        Triangle triangle = new Triangle(triangleSide,triangleSide,triangleSide);

        String triangleType = triangle.determineTriangleType();

        assertThat(triangleType, is("Equilateral"));

    }

    @Test
    public void atLeaseTwoSidesAreOfEqualLengthThenEquilateral() throws Exception {

        int triangleSide = 100;
        int differentLengthSide = 50;

        Triangle triangle = new Triangle(triangleSide, triangleSide, differentLengthSide);

        String triangleType = triangle.determineTriangleType();

        assertThat(triangleType, is("Isosceles"));
    }

    @Test
    public void allSidesAreOfDifferentLengthThenScalene() throws Exception {
        Triangle triangle = new Triangle(100, 90, 80);

        String triangleType = triangle.determineTriangleType();

        assertThat(triangleType, is("Scalene"));
    }

    @Test
    public void determiningTypeOfTriangleContainsZeroLengthOfAnySideThrowsException() throws Exception {
        Triangle triangle = new Triangle(100, 90, 0);

        exception.expect(Exception.class);

        triangle.determineTriangleType();
    }

    @Test
    public void determiningTypeOfTriangleContainsNegativeLengthOfAnySideThrowsException() throws Exception {
        Triangle triangle = new Triangle(100, -1, 80);

        exception.expect(Exception.class);

        triangle.determineTriangleType();
    }

    @Test
    public void determineSideOfTriangleContainsTwoSidesWhichSumIsLessThanThirdSideThenThrowsException() throws Exception {
        Triangle triangle = new Triangle(78 , 1, 80);

        exception.expect(Exception.class);

        triangle.determineTriangleType();
    }

}
