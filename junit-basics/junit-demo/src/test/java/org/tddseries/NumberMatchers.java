package org.tddseries;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

public class NumberMatchers {

	public static IsDivisibleByMatcher 																																																																																																																																																																																																																													isDivisibleBy(int divisor) {
		return new IsDivisibleByMatcher(divisor);
	}
	
	private static class IsDivisibleByMatcher extends TypeSafeDiagnosingMatcher<Integer> {

		private int divisor;

		public IsDivisibleByMatcher(int divisor) {
			this.divisor = divisor;
		}

		@Override
		public void describeTo(Description descirption) {
			descirption.appendText("Divisble by ").appendValue(divisor);
		}

		@Override
		protected boolean matchesSafely(Integer actualInteger, Description description) {
			if ((actualInteger % divisor) == 0) {
				return true;
			}
			description.appendText("was not divisible by").appendValue(divisor);
			return false;
		}
		
	}
}
