package org.tddseries;

import java.util.concurrent.TimeUnit;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class TimeoutRuleDemo {

	@Rule public Timeout timeout = new Timeout(1, TimeUnit.SECONDS);
	
	@Test
	public void failsBecauseOfTimeout() {
		for (;;);
	}
}
