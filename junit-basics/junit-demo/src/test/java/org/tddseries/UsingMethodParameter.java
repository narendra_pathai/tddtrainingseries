package org.tddseries;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;


@RunWith(JUnitParamsRunner.class)
public class UsingMethodParameter {

    public Object[][] data_provider_for_assignedTestAssignedGradeAccordingToPercentageReceived() {
        return new Object[][]{
                {100, Grade.A_PLUS},
                {91, Grade.A_PLUS},

                {90, Grade.A},
                {81, Grade.A},

                {80, Grade.A_MINUS},
                {71, Grade.A_MINUS},

                {70, Grade.B_PLUS},
                {61, Grade.B_PLUS},

                {60, Grade.B},
                {51, Grade.B},

                {50, Grade.B_MINUS},
                {35, Grade.B_MINUS},

                {34, Grade.F},
                {0, Grade.F}
        };
    }

    @Test
    @Parameters(method = "data_provider_for_assignedTestAssignedGradeAccordingToPercentageReceived")
    public void assignedTestAssignedGradeAccordingToPercentageReceived(int marks, Grade expectedGrade) {

        Grader grader = new Grader(100);

        Grade assignedGrade = grader.grade(marks);

        assertThat(assignedGrade, equalTo(expectedGrade));

    }


}
