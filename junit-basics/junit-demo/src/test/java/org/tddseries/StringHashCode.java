package org.tddseries;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class StringHashCode {

    private String dept = "CSM";

    @Test
    public void isConsistence() {
        assertThat(dept.hashCode(), allOf(equalTo(dept.hashCode()), equalTo(dept.hashCode()), equalTo(dept.hashCode())));
    }

    @Test
    public void twoEqualStringProduceSameHashCode() {
        assertThat(dept.hashCode(), equalTo("CSM".hashCode()));
    }


}
