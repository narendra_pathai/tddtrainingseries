package org.tddseries;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class BankAccountTest {

    private BankAccount bankAccount;

    @Before
    public void createBankAccount() {
        bankAccount = new BankAccount(5000, 1000);
    }


    @Test
    public void depositingAmountIncrementsTheBalanceByAmountDeposited() {

        // Arrange
        long currentBalance = bankAccount.getBalance();

        int depositAmount = 1000;

        // Act
        bankAccount.deposit(depositAmount);

        // Assert
        assertThat(bankAccount.getBalance(), equalTo(currentBalance + depositAmount));
    }

    @Test
    @Ignore
    public void withdrawingAmount_WhenBalancePostWithdrawalWillBeLessThanMinimumBalance_ThrowsException() {
        fail("Still need to implement");
    }


    @Test
    public void withdrawingAmountFromBankAccountWillDecreaseTheBalanceByAmountWithdrawn() throws Exception {

        long currentBalance = bankAccount.getBalance();

        int withdrawalAmount = 1000;

        bankAccount.withdraw(withdrawalAmount);

        assertThat(bankAccount.getBalance(), equalTo(currentBalance - withdrawalAmount));
    }



}
