package org.tddseries;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class JunitRunOrderDemo {

	public JunitRunOrderDemo() {
		System.out.println("#Constructor Junit run order demo created. Created once per test");
	}
	
	@BeforeClass
	public static void setUpBeforeClass() {
		System.out.println("#setUpBeforeClass Called once per test class");
	}
	
	@Before
	public void setUp() {
		System.out.println("#setUp Called once per test");
	}
	
	@Test
	public void thisIsATest() {
		System.out.println("#thisIsATest A test executed");
	}
	
	@Test
	public void thisIsOtherTest() {
		System.out.println("#thisIsOtherTest Other test executed");
	}
	
	@After
	public void tearDown() {
		System.out.println("#tearDown Called once per test");
	}
	
	@AfterClass
	public static void tearDownBeforeClass() {
		System.out.println("#tearDownAfterClass called once per test class");
	}
}
