package org.tddseries;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

public class PersonMatchers {

	
	public static AgeMatcher hasAge(int expectedAge) {
		return new AgeMatcher(expectedAge);
	}

	public static AdultMatcher isAdult() {
		return new AdultMatcher();
	}
	
	private static class AgeMatcher extends TypeSafeDiagnosingMatcher<Person> {

		private int expectedAge;

		public AgeMatcher(int expectedAge) {
			this.expectedAge = expectedAge;
		}

		@Override
		public void describeTo(Description description) {
			description.appendText("A person with age of ").appendValue(expectedAge).appendText(" years");
		}

		@Override
		protected boolean matchesSafely(Person actualPerson, Description description) {
			if (actualPerson.getAge() != expectedAge) {
				description.appendText("was ").appendValue(actualPerson.getAge()).appendText(" years");
				return false;
			}
			return true;
		}
		
	}

	private static class AdultMatcher extends TypeSafeDiagnosingMatcher<Person> {

		@Override
		public void describeTo(Description description) {
			description.appendText("A person with age of ").appendValue(18).appendText(" years or above");
		}

		@Override
		protected boolean matchesSafely(Person actualPerson, Description description) {
			if (actualPerson.isAdult() == false) {
				description.appendText("was ").appendValue(actualPerson.getAge()).appendText(" years");
				return false;
			}
			return true;
		}

	}
}
