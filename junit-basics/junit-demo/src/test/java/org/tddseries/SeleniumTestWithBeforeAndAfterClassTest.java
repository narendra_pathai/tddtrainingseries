package org.tddseries;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumTestWithBeforeAndAfterClassTest {

	private static WebDriver driver;
	private GoogleSearchPage googleHomePage;

	@BeforeClass
	public static void openWebDriver() {
		System.setProperty("webdriver.chrome.driver", "/media/narendra/WORK/LIBRARIES_AND_UTILS/chromedriver");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}
	
	@Before
	public void openGooglePage() {
		googleHomePage = new GoogleSearchPage(driver);
		googleHomePage.open();
	}

	@Test
	public void agileDataLinkIsTheFirstSearchResult() throws InterruptedException {
		googleHomePage.searchFor("Test Driven Development");

		String firstResult = googleHomePage.searchResults().findFirst().get();

		Assert.assertEquals("Introduction to Test Driven Development (TDD) - Agile Data", firstResult);
	}

	@Test
	public void wikipediaLinkIsTheSecondSearchResult() throws InterruptedException {
		googleHomePage.searchFor("Test Driven Development");

		String firstResult = googleHomePage.searchResults().skip(1).findFirst().get();

		Assert.assertEquals("Test-driven development - Wikipedia, the free encyclopedia", firstResult);
	}

	@AfterClass
	public static void closeWebDriver() {
		driver.close();
	}
}
