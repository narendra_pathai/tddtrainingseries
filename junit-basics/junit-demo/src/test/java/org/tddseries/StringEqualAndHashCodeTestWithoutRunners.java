package org.tddseries;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
public class StringEqualAndHashCodeTestWithoutRunners {


    private String dept = "CSM";

    @Test
    public void equals_InstanceWithItself_ReturnsTrue() {
        assertTrue(dept.equals(dept));
    }

    @Test
    public void equalsIsReflexive() {
        assertTrue(dept.equals(dept));
    }

    @Test
    public void equals_TwoStringsWithSameContent_ReturnsTrue() {
        assertEquals(dept.equals("CSM"), "CSM".equals(dept));
    }

    @Test
    public void equalsStringWithOtherInstanceContainsDiffentCharacterViceVersaReturnsTrue() {
        assertThat(dept.equals("CSMs"), CoreMatchers.sameInstance("CSMs".equals(dept)));
    }

    @Test
    public void equalsIsSymmetric() {
        assertThat(dept.equals("CSM"), CoreMatchers.sameInstance("CSM".equals(dept)));
        assertThat(dept.equals("CSMs"), CoreMatchers.sameInstance("CSMs".equals(dept)));
    }

    @Test
    public void equalsStringWithNullReturnFalse() {
        assertFalse(dept.equals(null));
    }

    @Test
    public void twoEqualStringReturnSameHashCode() {
        assertThat(dept.hashCode(), equalTo("CSM".hashCode()));
    }

    @Test
    public void multipleExecutionOfHashCoreProduceSameResult() {
        assertThat(dept.hashCode(), allOf(equalTo(dept.hashCode()), equalTo(dept.hashCode()), equalTo(dept.hashCode())));
    }

    @Test
    public void hashCodeIsConsistent() {
        assertThat(dept.hashCode(), allOf(equalTo(dept.hashCode()), equalTo(dept.hashCode()), equalTo(dept.hashCode())));
    }

    public void equalsWithNullReturnFalse() {
        assertFalse(dept.equals(null));
    }

}
