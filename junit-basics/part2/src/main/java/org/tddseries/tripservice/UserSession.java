package org.tddseries.tripservice;

public class UserSession {
	
	private static final UserSession INSTANCE = new UserSession();
	
	public static UserSession getInstance() {
		return INSTANCE;
	}
	
	public User getLoggedUser() {
		throw new CallToDependencyFromUnitTestException("User session class should not be called during unit testing.");
	}
}
