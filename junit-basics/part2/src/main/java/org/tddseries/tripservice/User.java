package org.tddseries.tripservice;

import java.util.ArrayList;
import java.util.List;

public class User {

	private final String name;
	private List<User> friends = new ArrayList<>();
	private List<Trip> trips = new ArrayList<>();

	public User(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void addTrip(Trip trip) {
		trips.add(trip);
	}
	
	public void addFriend(User friend) {
		friends.add(friend);
	}
	
	public List<User> getFriends() {
		return friends;
	}

	public List<Trip> getTrips() {
		return trips;
	}
}
