package org.tddseries.tripservice;

public class CallToDependencyFromUnitTestException extends RuntimeException {

	public CallToDependencyFromUnitTestException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 1L;

}
