package org.tddseries.tripservice;

import java.util.ArrayList;
import java.util.List;

public class TripService {

	public List<Trip> findTripsByUser(User user) throws UserNotLoggedInException {
		List<Trip> tripList = new ArrayList<>();
		boolean isFriend = false;
		User loggedInUser = UserSession.getInstance().getLoggedUser();
		if (loggedInUser != null) {
			for (User friend : user.getFriends()) {
				if (friend.equals(loggedInUser)) {
					isFriend = true;
					break;
				}
			}
			
			if (isFriend) {
				tripList = TripDAO.findTripsByUser(user);
			}
			return tripList;
		} else {
			throw new UserNotLoggedInException();
		}
	}
}
